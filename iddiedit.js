/**
* IDDI - Front End Suff
* All of this is copyright J.Patchett / Tastic Multimedia - No part of this code may be reproduced in any form without the written permission of the copyright ownder
**/

if (typeof formdata != 'undefined')
    var formdata;

var editorframe=null;
var mywindow=this;

var ie=(document.all);

//Iddi main - attach adds the provided data to the page and performs initial setup
var iddi = {
    //enable console logging
    debug:false,
    //flag to let us know setup has been done
    setupdone:false,
    //virtual filename of the current file being edited
    virtualfilename:'',
    //Link to the uiframe - same as $ or jQuery but for clarity we use uiframe - the method editframe returns a link to the jQuery in the edit iframe
    uiframe:null,
    //Master list of editable fields on this page
    datafields:{},
    last_uploaded_file:'',
    //Attaches data to the page
    reset:function(){
        iddi.datafields={};
    },
    attach:function(data){
        //Check we're at the top - if not pass this info on to the top
        if(mywindow!=top) top.iddi.attach(data);

        //Console logging
        if (iddi.debug) console.log('Getting Data');

        //Show the overlay to the user
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'),'Loading Data');

        //Merge the data with existing data
        if (typeof data != 'undefined') $.extend(formdata,data);

        //Start the editor if we need to
        if (!iddi.setupdone) iddiEditor.startEditor();

        if (typeof formdata != 'undefined'){

            //Create the ivals - these can then be attached to named fields or to linked elements
            for(va in formdata.values){
                v=formdata.values[va];
                v.setValue=function(value){
                    this.changed=true;
                    this.value=value;
                    iddiEditor.mark_changed();
                };
                n='iddifield_'+v.postfieldid;
                //Store in the master field lookup table
                //iddi.datafields[va]=v;
                iddi.editframe('*[name="'+n+'"],.'+n).each(function(){if(this) jQuery(this).iddiStuff(v)});
                iddi.uiframe('*[name="'+n+'"],.'+n).each(function(){if(this) jQuery(this).iddiStuff(v)});

                if(v.fieldname=='templatefile' && v.value!=iddiEditor.currenttemplate){
                    if (iddiEditor.currenttemplate!='') iddiEditor.reloadMain();
                    iddiEditor.currenttemplate=v.value;
                }
                if(v.fieldname=='virtualfilename') iddi.virtualfilename=v.value;
                if(v.fieldname=='basefilename') iddi.basefilename=v.value;
                if(v.fieldname=='id') iddi.entityid=v.value;

            }

            //Attach ivals to linked elements
            for(link in formdata.fieldlinks){
                v=formdata.fieldlinks[link];
                if(v!=null){
                  va=formdata.values[v.postfieldid];
                  va.setValue=function(value){
                      iddiEditor.mark_changed();
                      this.changed=true;
                      this.value=value;
                  };
                  //Store in the master field lookup table
                  //iddi.datafields[v]=va;
                  if (typeof va != 'undefined'){
                    if(iddi.editframe('#'+link).length>0){
                      if (iddi.editframe('#'+link+' select').length>0){
                          iddi.editframe('#'+link+' select').iddiStuff(va).addClass('iddi-editable');
                      }else{
                          if(iddi.editframe('#'+link+' input').length>0){
                              iddi.editframe('#'+link+' input').iddiStuff(va).addClass('iddi-editable');
                          }else{
                              iddi.editframe('#'+link).iddiStuff(va).addClass('iddi-editable');
                          }
                      }
                    }
                  }
                }
            }
        }
        //Attach editors to images in editable areas
        iddi.editframe('.iddi-editable img').iddiMakeEditableImage();

        //Hide the loader
        iddiui.hideLoader(iddi.uiframe('#iddi-overlay'));
        //Quick message to the console
        if (iddi.debug) console.log('Getting Data Done');
    },
    //Loads data from the server
    getformdata:function(){
        //Show an overlay to prevent editing while we update the data
        iddiui.showoverlay();
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'),'Getting Data');
        //Grab the data from the server, on success we run attach and hide the overlay
        iddiAjaxCaller("/iddi/iddi.php",
        function(data){
          iddi.attach(data);
          iddiui.hideLoader(iddi.uiframe('#iddi-overlay'));
          iddiui.hideoverlay();
        },null,'getform');
    },
    //Gets reference to jQuery direct on the editor iframe
    editframe:function(selector,context){
        try{
            return frames[0].window.$(selector,context);
        }catch(e){
            return $(selector,context);
        }
    }
};


//iddiui provides UI functions such as tool loading, popup loading and overlays
var iddiui={
    //Counter - lets us assign a new id to each popup
    popupid:1,
    //Zindex counter - means the latest popup will always sit on top
    zindex:10000,
    onShowHandlers:{},
    onHideHandlers:{},
    //Just show the overlay
    showoverlay:function(){ iddi.uiframe('#iddi-overlay').css('display','block').stop().fadeTo('medium',0.7); },
    //And hide it
    hideoverlay:function(){ iddi.uiframe('#iddi-overlay').stop().fadeOut('fast'); },
    //Show the iddi toolbars and edit panes
    showIddi:function(){
        iddiEditor.editorenabled=true;
        for(h in iddiui.onShowHandlers) iddiui.onShowHandlers[h]();
    },
    //Hide the iddi toolbars and edit panes
    hideIddi:function(){
        iddiEditor.editorenabled=false;
        iddiEditor.switchOffEditMode();
        iddiEditor.unShowEditableAreas();
        for(h in iddiui.onHideHandlers) iddiui.onHideHandlers[h]();
    },
    //Brings the passed in object to the front of the stack if possible
    bringToFront:function(obj){
        obj.css('z-index',iddiui.zindex++);
    },
    //Shows the loading graphic with an optional message
    showLoader:function(obj,message){
        //document.designMode='off';
        obj.html('');
        obj.css('background-image','url(/iddi/ajax-loader.gif)');
        obj.css('background-repeat','no-repeat');
        obj.css('background-position','50% 50%');
        if(typeof message != 'undefined'){
            obj.append('<div id="loader-message" style="position:absolute;top:50%;padding-top:50px;background:white;width:100%;text-align:center;font-size:2em;margin-top:-0.5em">'+message+'</div>');
        }

    },
    //Hides the loader graphic and message
    hideLoader:function(obj){
        obj.css('background-image','');
        iddi.uiframe('#loader-message').remove();
    },
    //Handles loading of a popup tool
    //Toolname = the name of the tool to load
    //Sourcefile = location of the xml file where the tool is stored
    loadPopupTool:function(toolname,sourcefile,imageeditor,more){
        if (typeof s == 'undefined') sourcefile='iddi-edit';
        if (typeof more == 'undefined') more='';
        iddiEditor.hideEditBounder();
        iddiui.popupid++;
        iddiui.showoverlay();
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'));
        iddi.uiframe('#iddi-stuff')
            .append('<div id="popup-holder-'+iddiui.popupid+'"/>')
            .find('#popup-holder-'+iddiui.popupid)
            .hide()
            .addClass('iddi')
            .iddiAjax('/iddi/iddi.php?',function(){
                iddi.uiframe('#popup-holder-'+iddiui.popupid)
                    .stop()
                    .fadeIn(400)
                    .draggable({handle:'h1'}).find('h1')
                    .append('<div class="iddi-close-button" title="Close Window"><span>X</span></div>')
                    .find('.iddi-close-button').click(function(){
                      iddiui.closePopup(iddiui.popupid);
                    });
                //Remove flash as dialogs won't appear over the top of it
                iddi.uiframe('embed').stop().fadeOut('fast');
                iddiui.hideLoader(iddi.uiframe('#iddi-overlay'));
                iddi.uiframe('#popup-holder-'+iddiui.popupid).iddiBringToFront();
                iddiEditor.setupAfterAjax();
                if (typeof imageeditor != 'undefined') imageeditor.onLoad(iddi.uiframe('#popup-holder-'+iddiui.popupid));
            },'tool',{source:sourcefile,tool:toolname});
    },
    //Returns a jquery object to the current popup window
    getCurrentPopup:function(){
        return iddi.uiframe('#popup-holder-'+iddiui.popupid);
    },
    closePopup:function(id){
      popup=iddi.uiframe('#popup-holder-'+id);
      popup.find('select').change(iddiEditor.valueChanged);
      popup.find('input').change(iddiEditor.valueChanged);
      popup.stop().fadeOut('medium',function(){
        popup.remove();
      });
      iddiui.hideoverlay();
      iddi.uiframe('embed').show('fast');
    },
    loadPopupReply:function(url){
        iddiui.popupid++;
        iddiui.showoverlay();
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'));
        iddi.uiframe('#iddi-stuff').append('<div class="iddi-popup" id="popup-holder-'+iddiui.popupid+'"><h1>Message</h1><div class="iddi-wizard-step-container"></div></div>')
                        .find('#popup-holder-'+iddiui.popupid+' .iddi-wizard-step-container')
                        .addClass('iddi')
                        .iddiAjax(url,function(){
                                iddi.uiframe('#popup-holder-'+iddiui.popupid).draggable({handle:'h1'}).find('h1').append('<div class="iddi-close-button"><span>X</span></div>').find('.iddi-close-button').click(function(){
                                    iddi.uiframe(this).parent().stop().fadeOut('fast');
                                    iddiui.hideoverlay();
                                    iddi.uiframe('#popup-holder-'+iddiui.popupid).remove();
            });
            iddiEditor.setupAfterAjax();
            iddiui.hideLoader(iddi.uiframe('#iddi-overlay'));
        });
    }
}

//Iddi core provides core functionality such as saving and field transfers
var iddiCore={
  _callback:null,
    //Save the current page
    save:function(callback){
        iddiEditor.hideEditBounder();
        _callback=callback;
        if(iddi.virtualfilename==null){
            iddiui.loadPopupTool('saveas-file-popup')
        }else{
            savem=new iddiSideMessage();
            savem.waitonajax=1;
            savem.show('information','Saving your work');
            iddiAjaxCaller('/iddi/iddi.php',iddiCore.saveHandler,null,'save');
        }
    },
    //Start the save as popup
    saveas:function(){
        iddiui.loadPopupTool('saveas-file-popup');
    },
    deep_copy:function(callback){
        iddiEditor.hideEditBounder();
        _callback=callback;
        if(iddi.virtualfilename==null){
            iddiui.loadPopupTool('deep-copy-popup')
        }else{
            savem=new iddiSideMessage();
            savem.waitonajax=1;
            savem.show('information','Copying Data');
            iddiAjaxCaller('/iddi/iddi.php',iddiCore.saveHandler,null,'deepcopy');
        }
    },
    //Handler for the completion of the ajax save
    saveHandler:function(data){
        var allok=false;
        savem.retire();
        if(data.errors){
          for(err in data.errors){
            error=data.errors[err];
                //msg+="\n"+error.type+' : '+error.message
                if (error.code=='iddi.data.saved') allok=true;
          }
        }
        if (!allok){
            var error={'type':'serious','message':'Your work was not saved','code':'iddi.data.notsaved'};
            m=new iddiSideMessage();
            m.show(error.type,error.message);
            if(typeof _callback != 'undefined') callback(false);
        }else{
          if(_callback!==undefined){
            iddiEditor.mark_nochanged();
            _callback(true);
          }else{
            iddiEditor.mark_nochanged();
            iddi.editframe(document)[0].location=iddi.editframe(document)[0].location;
          }
        }
        _callback=null
    },
    //Save the current page
    delete_page:function(){
      if(confirm('Are you sure you want to delete this page?')){
        savem=new iddiSideMessage();
        savem.waitonajax=1;
        savem.show('information','Deleting Page');
        iddiAjaxCaller('/iddi/iddi.php',function(){
          top.document.location=top.document.location;
        },null,'delete');
      }
    },
    restore_page:function(){
      if(confirm('Are you sure you want to restore this page?')){
        savem=new iddiSideMessage();
        savem.waitonajax=1;
        savem.show('information','Restoring Page');
        iddiAjaxCaller('/iddi/iddi.php',function(){
          top.document.location=top.document.location;
        },'restore');
      }
    },
    serverSync:function(callback){
        var updates=iddiCore.getUpdates(false);
        if (updates){
            //updates=encodeURIComponent(updates);
            post={fieldupdate:updates};
            iddi.uiframe.post('/iddi/iddi.php?context=getform',post);
            iddiAjaxCaller("/iddi/iddi.php",
            function(data){
              iddi.attach(data);
            },null,'getform');
            if(typeof callback != 'undefined') callback();
        }
    },
    //Builds JSON to send to the server - this needs some serious work !
    getUpdates:function(all){
        if(typeof all == 'undefined') all=false;
        var updates='{';
        var firstupdate='"values":{';
        var pdate='';
        var comma='';
        var updateno=1;
        if (typeof formdata != 'undefined'){
            for(link in formdata.values){
                v=formdata.values[link];
                if (all==true || v.changed==true){
                    thisvalue=v.value;
                    formdata.values[link].changed=false;
                    if(typeof thisvalue == 'string'){
                        //thisvalue=thisvalue.replace(/</g,'&lt;');
                        //thisvalue=thisvalue.replace(/>/g,'&gt;');
                        thisvalue=thisvalue.replace(/\{/g,'&curlyopen;');
                        thisvalue=thisvalue.replace(/\}/g,'&curlyclose;');
                        thisvalue=thisvalue.replace(/\n/g,'&newline;');
                        thisvalue=thisvalue.replace(/<br>/g,'<br/>');
                        thisvalue=thisvalue.replace(/"/g,'\\"');
                        thisvalue=encodeURIComponent(thisvalue);
                        //thisvalue=thisvalue.replace(/\%/g,'entpct');
    //                    thisvalue=thisvalue.replace(/'/g,'\\\'');
                        updates+=firstupdate+comma+'"'+updateno+'":{"postfieldid":"'+v.postfieldid+'","value":"'+thisvalue+'"}';
                        pdate="}";                          //Because we do have some updates we need to add a close } at the end
                        comma=',';                          //This is used to prefix any further updates so we have a comma seperated list
                        v.changed=false;
                        firstupdate='';                     //No Longer first update - so we can stop adding it
                        updateno++;
                    }
                }
            }
        }
        updates+=pdate+'}';
        return updates;
    }
}

function iddiColumnFileManager(inPopup){
    var levelload=0;
    var maxlevel=0;
    this.langfilter='en-gb';
    this.toolname='virtualfiles';
    var _popup=inPopup;
    var _self=this;
    var _beforeajax=null;
    var _fileclick=null;

    this.setup=function(){
        _popup.find('.openfile-cmd-open:not(.jax)').click(function(){
            iddiEditor.load(_popup.find('.filename').val());
        }).addClass('jax');
        _popup.find('.open-filter-language:not(.jax)').change(function(){
            _self.langfilter=iddi.uiframe(this).val();
            _self.starttree();
        }).addClass('jax');
        //_popup.find('.file-picker select option:not(.jax)').dblclick(function(){iddiEditor.load(iddi.uiframe(this).val()); });
        //_popup.find('.file-picker select:not(.jax)').change(function(){alert('test');}).addClass('jax');
        _popup.find('.file-picker select:not(.jax)').click(function(){
            iddi.uiframe(this).addClass('jax');
            if(_self._fileclick!=null) _self._fileclick(_self,this);
            var level=iddi.uiframe(this).attr('rel');
            level++;
            var tgt=iddi.uiframe(this).val();
            if(tgt=='/') tgt='';
            var url='/iddi/iddi.php?ajax=1&context=tool&source=iddi-edit&tool=child-'+_self.toolname+'&level='+level+'&path='+tgt+'&langfilter='+_self.langfilter;
            //if(levelload
            //Create a new level, then load some content
            if(level > maxlevel){
                _popup.find('.file-picker').append('<div class="level'+level+'"></div>');
                maxlevel=level;
            }
            if(level < levelload){
                _popup.find('.file-picker').stop().animate({'scrollLeft':(level*180)+'px'},600);
                for(a=level+1;a<levelload+1;a++){
                    _popup.find('.file-picker .level'+a).stop().fadeOut('medium');
                }
            }
            //Plonk the help in the chooser help area if there is one
            _popup.find('.chooser-help').html(iddi.uiframe(this)[0].helptag);
            levelload=level;
            _popup.find('.filename').val(iddi.uiframe(this).val());

            if(_self._beforeajax!=null) _self._beforeajax(_self);

            _popup.find('.file-picker .level'+level).iddiAjax(url,_self.treescroller);

        });
        _popup.find('.file-picker select option:not(.jax)').mouseover(function(){
            //Plonk the help in the chooser help area if there is one
            _popup.find('.chooser-help').html(iddi.uiframe(this)[0].helptag);
        }).addClass('jax');
    };

    this.selectpath=function(path){
        alert(path);
    }

    this.treescroller=function(){
        _self.setup();
        iddi.attach();
        _popup.find('.file-picker .level'+levelload).stop().fadeIn('medium');
        _popup.find('.file-picker').stop().animate({'scrollLeft':(levelload*180)+'px'},600);
    };

    this.starttree=function(){
        _self.langfilter=_popup.find('.open-filter-language').val();
        var url='/iddi/iddi.php?ajax=1&context=tool&source=iddi-edit&tool=first-'+_self.toolname+'&level=0&path=&langfilter='+_self.langfilter;
        for(a=1;a<levelload+1;a++){
            _popup.find('.file-picker .level'+a).stop().fadeOut('medium');
        }
        levelload=0;
        _popup.find('.file-picker .level0').iddiAjax(url,this.treescroller);
        _self.setup();
    };
}

//Creates a new column file manager
$.fn.iddiCreateColumnFileManager=function(){
    if (typeof this[0].filemanager == 'undefined'){
        this[0].filemanager=new iddiColumnFileManager(iddi.uiframe(this));
        this[0].filemanager._popup=iddi.uiframe(this);
        this[0].filemanager.starttree();
    }
    return this[0].filemanager;
}
$.fn.iddiGetFileManager=function(){
    if (typeof this[0].filemanager != 'undefined'){
            return this[0].filemanager;
    }else{
        if(this[0].nodeName!='#document'){
            return jQuery(this).parent().iddiGetFileManager();
        }else{
            return null;
        }
    }
}
$.fn.iddiCreateColumnEntityManager=function(){
    if (typeof this[0].filemanager == 'undefined'){
        this[0].filemanager=new iddiColumnFileManager(iddi.uiframe(this));
        this[0].filemanager._popup=iddi.uiframe(this);
        this[0].filemanager.toolname='entityloader';
        this[0].filemanager.starttree();
    }
    return this[0].filemanager;
}


var iddiImageEditor={
    editor:null,
    target:null,
    once:0,
    load_message:null,
    onLoad:function(editor){
        this.editor=editor;
        iddiImageEditor.once=0;
        iddiEditor.hideEditBounder();
        editor.find('img#image-editor-popup-image:not(.jax8)').addClass('jax8').attr('src',this.target.attr('src'));
        editor.find('img:not(.jax7)').addClass('jax7').click(this.updateImageSrc);
        editor.find('input:not(.jax6)').addClass('jax6').change(this.updateImage);
        editor.find('#library:not(.jax5)').addClass('jax5').change(this.showLibrary);
        editor.find('#cmd-image-edit-remove-image:not(.jax4)').addClass('jax').click(this.removeimage);
        editor.find('#uploadform:not(.jax3)').addClass('jax3').submit(function(){
          iddiImageEditor.load_message=new iddiSideMessage();
          iddiImageEditor.load_message.donthide=true;
          iddiImageEditor.load_message.show('progress','Uploading Image');
          editor.find('#browse_link').val('Uploading');
        }),
        editor.find('#imageuploadframe:not(.jax2)').addClass('jax2').load(function(){
          if(iddiImageEditor.once!=0){
            iddiImageEditor.load_message.retire();
            m=new iddiSideMessage();
            m.show('success','Image uploaded.');
            m=new iddiSideMessage();
            m.show('success','Image automatically attached for you.<br/>Please close the image library to continue or choose another');
            iddiImageEditor.setImageSrc(iddi.last_uploaded_file);
            editor.find('#browse_link').val('Add Image');
            editor.find('#browse_link').click();
            iddiImageEditor.showLibrary();
          }
          iddiImageEditor.once=1;
        }),
        iddi.uiframe('#add_library:not(.jax1)').addClass('jax1').click(function(){
            var v=prompt('Enter a name for the new library');
            if (v){
                re = new RegExp('[^A-Za-z0-9_ /]','gim');
                v=v.replace(re,'');
                re = new RegExp('[ ]','gim');
                v1=v.replace(re,'_');
                v1=v1.replace('//','/');
                v1=v1.replace('__','_');
                v1=v1.replace('__','_');
                v1=v1.replace('__','_');
                m=new iddiSideMessage();
                m.show('success','Library Created');
                iddi.uiframe('#library').append('<option value="'+v1+'">'+v+'</option>');
                //iddi.uiframe('#create_library').text(v);
                //iddi.uiframe('#create_library').val(v);
                iddi.uiframe('#library').val(v).change();
            }else{
                return false;
            }
        });

    },
    removeimage:function(){
        if (typeof iddiImageEditor.target[0].ival != 'undefined'){
            iddiImageEditor.target[0].ival.setValue('general/no-pic.jpg');
            iddiCore.serverSync();
        }else{
            alert("ERROR: Can't update image value");
        }
    },
    updateImage:function(){
        image=this.value;
        if (image==''){
            iddiImageEditor.target.attr('src','');
        }else{
            iddiImageEditor.target.attr('src',image);
        }
    },
    updateEditorImage:function(){
        image=this.value;
        if (image==''){
            iddi.uiframe('#image-editor-popup-image').attr('src','');
        }else{
            iddi.uiframe('#image-editor-popup-image').attr('src',image);
        }
    },
    updateImageSrc:function(){
        image=jQuery(this).attr('rel');
        iddiImageEditor.setImageSrc(image);
    },
    setImageSrc:function(image){
      //iddi.uiframe('#image-editor-popup-image').attr('src',image);
        iddiImageEditor.target.attr('src','/iddi/pixel.png').css('background','url(/iddi/status-progress.gif) no-repeat 50% 50%');
        if (typeof iddiImageEditor.target[0].ival != 'undefined'){
            iddiImageEditor.target[0].ival.setValue(image);
            iddiCore.serverSync();
            iddiImageEditor.target[0].ival.setValue(image);
            iddiCore.serverSync();
        }else{
          if (typeof iddiImageEditor.target != 'undefined'){
             iddiImageEditor.target[0].src=image;
             iddiImageEditor.target[0].src=image;
          }else{
            alert("ERROR: Can't update image value");
          }
        }
    },
    showLibrary:function(){
        var lib = iddi.uiframe('#library').val();
        if (lib!=''){
            iddiui.showLoader(iddi.uiframe('#iddi-image-chooser-popup #file-picker'));
            iddi.uiframe('#iddi-image-chooser-popup #file-picker').iddiAjax('/iddi/iddi.php',function(){
                                    iddiEditor.setupAfterAjax();
                                    iddi.uiframe('#iddi-image-chooser-popup #file-picker img').click(iddiImageEditor.updateEditorImage);
                                    iddi.uiframe('#iddi-image-chooser-popup #file-picker img').click(iddiImageEditor.updateImageSrc);
                                    iddiui.hideLoader(iddi.uiframe('#iddi-image-chooser-popup #file-picker'))
                                },'tool',{source:'iddi-edit',tool:'image-editor-popup',section:'file-picker',library:lib});
        }
    },
    generateImage:function(image){
      if(typeof image[0].iddiImageEdit !='undefined'){
        image[0].iddiImageEdit.currentWidth=image[0].width;
        image[0].iddiImageEdit.currentHeight=image[0].height;
        //image[0].iddiImageEdit.currentLeft=image[0].offsetLeft;
        if (typeof image[0].sourceimage == 'undefined') image[0].sourceimage=image.attr('src');
        if (image[0].iddiImageEdit.currentWidth!=image[0].iddiImageEdit.lastWidth || image[0].iddiImageEdit.currentHeight!=image[0].iddiImageEdit.lastHeight) {
            //Generate a new image
            //image.attr('width',image[0].width);
            //image.attr('height',image[0].height);
            image.attr('alt','');
//            {"2667":{"resize":{"width":"158","height":"205","scalemode":"force"}}}
//            a:1:{i:2667;a:1:{s:6:"resize";a:3:{s:5:"width";s:3:"158";s:6:"height";s:3:"205";s:9:"scalemode";s:5:"force";}}}
            var instructions='a:1:{i:0;a:1:{s:6:"resize";a:3:{s:5:"width";i:'+image[0].width+';s:6:"height":i:'+image[0].height+';s:9:"scalemode";s:4:"crop";}}}';
            $.get('/iddi/iddi.php?context=generateimage&src='+encodeURIComponent(image[0].sourceimage)+'&instructions='+encodeURIComponent(instructions),null,function(data){
                image.attr('src',data);
                image.iddiGetEditableParent()[0].ival.value=image.iddiGetEditableParent().html();
                image.iddiGetEditableParent()[0].ival.changed=true;
                iddiEditor.mark_changed();
            });
        }
        image[0].iddiImageEdit.lastWidth=image[0].width;
        image[0].iddiImageEdit.lastHeight=image[0].height;
        //image[0].iddiImageEdit.lastLeft=image[0].offsetLeft;
      }
//        document.title=image.width;
    }
}

var iddiFileAttach={
    editor:null,
    target:null,
    once:0,
    load_message:null,
    onLoad:function(editor){
        this.editor=editor;
        iddiFileAttach.once=0;
        iddiEditor.hideEditBounder();
        editor.find('input:not(.jax)').addClass('jax').change(this.updateImage);
        editor.find('#library:not(.jax)').addClass('jax').change(this.showLibrary);
        editor.find('#cmd-image-edit-remove-image:not(.jax)').addClass('jax').click(this.removeimage);
        editor.find('#uploadform:not(.jax)').addClass('jax').submit(function(){
          iddiFileAttach.load_message=new iddiSideMessage();
          iddiFileAttach.load_message.donthide=true;
          iddiFileAttach.load_message.show('progress','Uploading File');
          editor.find('#browse_link').val('Uploading');
        }),
        editor.find('#imageuploadframe:not(.jax)').addClass('jax').load(function(){
          if(iddiFileAttach.once!=0){
            if(iddi.last_uploaded_file!='' && iddi.last_uploaded_file!='undefined'){
                iddiFileAttach.showLibrary();
                iddiEditor.command('createlink',iddi.last_uploaded_file);
                iddiui.closePopup(iddiui.popupid);
                iddiFileAttach.load_message.retire();
                m=new iddiSideMessage();
                m.show('success','File uploaded.');
                m=new iddiSideMessage();
                m.show('success','File automatically attached for you');
                iddi.last_uploaded_file='';
            }
          }
          iddiFileAttach.once=1;
        }),
        editor.find('#actionframe:not(.jax)').addClass('jax').load(function(){
          if(iddiFileAttach.once!=0){
            iddiFileAttach.showLibrary();
          }
          iddiFileAttach.once=1;
        }),
        iddi.uiframe('#add_library:not(.jax)').addClass('jax').click(function(){
            var v=prompt('Enter a name for the new library');
            if (v){
                re = new RegExp('[^A-Za-z0-9_ /]','gim');
                v=v.replace(re,'');
                re = new RegExp('[ ]','gim');
                v1=v.replace(re,'_');
                v1=v1.replace('//','/');
                v1=v1.replace('__','_');
                v1=v1.replace('__','_');
                v1=v1.replace('__','_');
                m=new iddiSideMessage();
                m.show('success','Library Created');
                iddi.uiframe('#library').append('<option value="'+v1+'">'+v+'</option>');
                //iddi.uiframe('#create_library').text(v);
                //iddi.uiframe('#create_library').val(v);
                iddi.uiframe('#library').val(v).change();
            }else{
                return false;
            }
        });

    },
    showLibrary:function(){
        var lib = iddi.uiframe('#library').val();
        if (lib!=''){
            iddiui.showLoader(iddi.uiframe('#iddi-attach-file-popup #file-picker'));
            iddi.uiframe('#iddi-attach-file-popup #file-picker-contain').iddiAjax('/iddi/iddi.php',function(){
                                    iddiEditor.setupAfterAjax();
                                    iddiui.hideLoader(iddi.uiframe('#iddi-attach-file-popup #file-picker'));
                                    iddiFileAttach.afterAjax();
                                },'tool',{source:'iddi-edit',tool:'attach-file',section:'file-picker',library:lib});
        }
    },
    afterAjax:function(){
                    iddi.uiframe('td.action > *').css({'opacity':0.2});
                    iddi.uiframe('#browse_image:not(.jax)').addClass('jax').change(function(){
                        $('#uploadform').submit();
                    });
                    iddi.uiframe('#file-picker td.action-add-link:not(.jax)').click(function(){
                      var v='/downloads/'+$(this).parent().attr('rel');
                      iddiEditor.command('createlink',v);
                      iddiui.closePopup(iddiui.popupid);
                    });
                    iddi.uiframe('#file-picker td.action-delete:not(.jax)').click(function(){
                      var v='/downloads/'+$(this).parent().attr('rel');
                      iddiFileAttach.deleteLibraryFile(v);
                    });
                    iddi.uiframe('#file-picker td:not(.jax)').addClass('jax').hover(function(){
                        iddi.uiframe(this).parent().find('td.action > *').stop().animate({'opacity':1},'fast');
                        iddi.uiframe(this).parent().css('background','#ddd');
                    },function(){
                        iddi.uiframe(this).parent().find('td.action > *').stop().animate({'opacity':0.2},400);
                        iddi.uiframe(this).parent().css('background','');
                    });
                    /*
                    iddi.uiframe('#file-picker td').click(function(){
                        if(iddi.uiframe(this).parent().hasClass('selected')){
                            iddi.uiframe(this).parent().removeClass('selected');
                        }else{
                            iddi.uiframe(this).parent().addClass('selected');
                        }
                    });*/
    },
    deleteLibraryFile:function(filename){
      iddiFileAttach.load_message=new iddiSideMessage();
      iddiFileAttach.load_message.donthide=true;
      iddiFileAttach.load_message.show('progress','Deleting File from Library');
      iddi.uiframe.get('/tools_delete_file?file='+filename,function(data){
          iddiFileAttach.showLibrary();
          iddiFileAttach.load_message.retire();
          m=new iddiSideMessage();
          if(data=='true'){
            m.show('success','File deleted');
          }else{
            m.show('failure','File was NOT deleted');
          }
      });
    }
}


function iddiEditableImage(){
    var sourceimage='blank';
    var lastHeight=0;
    var lastWidth=0;
    var lastLeft=0;
    var currentHeight=0;
    var currentWidth=0;
    var currentLeft=0;
}

var iddiHelp={
    chelp:null,
    resetTimer:null,
    ev:null,
    wait:1500,
    showHoverHelp:function(e){
        var _self=this;
        _self.ev=e;
        if(this.chelp) clearTimeout(this.chelp);
        iddiHelp.doHelp(e);
        this.chelp=setTimeout(function(){iddiHelp.showHelp(_self.ev)},iddiHelp.wait);
    },
    cancelHoverHelp:function(){
        if(this.chelp) clearTimeout(this.chelp);
    },
    showHelp:function(e){
        iddiHelp.wait=300;
        //iddi.uiframe('#iddi-help-floater').animate({'opacity':'1'},400);
    },
    resetHelpTimer:function(){
        iddiHelp.wait=1500;
    },
    doHelp:function(e){
        if (iddiEditor.editorenabled){
            iddi.uiframe('#iddi-help-floater').css('opacity','0');
            var control=iddi.uiframe(e.target);
            var used=false;
            if (control.attr('title')!=''){
                control[0].helptag=control.attr('title');
                control.removeAttr('title');
            }
            if (typeof control[0].helptag!='undefined'){
                iddi.uiframe('#iddi-help-floater').show();
                iddi.uiframe('#iddi-help-floater p').html(control[0].helptag);
                used=true;
            }else{
                if (control.iddiIsEditable() && control.css('opacity')>0 && control.css('display')!='none' && control.css('visiblity')!='hidden'){
                    used=true;
                    var control2=control.iddiGetEditableParent();
                    if (control2 != null) control=control2;
                    iddi.uiframe('#iddi-help-floater').show();
                    var e='';
                    var p='';
                    if (typeof control[0].ival != 'undefined'){
                        if (typeof control[0].ival.HINT != 'undefined'){
                            e+=control[0].ival.HINT+' : ';
                        }else{
                            e+=control[0].ival.NAME+' : ';
                        }
                        p=" (part of "+control[0].ival.entityname+" number "+control[0].ival.entityid+")";
                    }
                    iddi.uiframe('#iddi-help-floater p').html(e+'You can edit this area'+p);
                }
            }
            if (used){
                if(iddiHelp.resetTimer) clearTimeout(iddiHelp.resetTimer);
                r=control[0].getBoundingClientRect();

                var left=r.left;
                var width=r.right-r.left;
                var my_class='above';


                if (width<200) width=200;
                if (left<0) left=0;

                iddi.uiframe('#iddi-help-floater').css('left',left+'px');
                iddi.uiframe('#iddi-help-floater').css('width',width+'px');
                a=iddi.uiframe('#iddi-help-floater')[0].getBoundingClientRect();

                //See if we want to adjust the width a bit
                if ((a.bottom-a.top)>60) iddi.uiframe('#iddi-help-floater').css('width',(650-r.left)+'px');

                a=iddi.uiframe('#iddi-help-floater')[0].getBoundingClientRect();
                t=iddi.uiframe('body')[0].scrollTop;
                var top=r.top+t-(a.bottom-a.top)-19;
                if (top<0){
                    top=r.bottom+t+19;
                    my_class='below';
                }
                iddi.uiframe('#iddi-help-floater').css('top',top+'px').removeClass('above').removeClass('below').addClass(my_class);
            }else{
                iddiHelp.resetTimer=setTimeout('iddiHelp.resetHelpTimer();',5000);
                if (control.attr('id')!='iddi-help-floater' && control.parent().attr('id')!='iddi-help-floater' && control.parent().parent().attr('id')!='iddi-help-floater') iddi.uiframe('#iddi-help-floater').hide();
            }
        }
    }
}

window.onbeforeunload = function() {
    if(iddiEditor.changed){
      return 'You have unsaved changes. Click CANCEL to stay on this page and then click on the "Save" icon to save your work. If you are happy to loose your changes click OK.';
    }
    iddiEditor.changed=false;
}

//Iddi editor provides all the functionality to make the editor only work when and where required.
var iddiEditor={
    currenteditnode:null,
    activeeditnode:null,
    currentSelection:null,
    current_edit_event:null,
    imageid:1,
    imageleftpos:true,
    editorenabled:true,
    changed:false,
    currenttemplate:'',
    mark_changed:function(){
      iddiEditor.changed=true;
      $('.iddi-cmd-save').removeClass('r1c1').addClass('r1c4');
    },
    mark_nochanged:function(){
      iddiEditor.changed=false;
      $('.iddi-cmd-save').removeClass('r1c4').addClass('r1c1');
    },
    valueChanged:function(e){
        if (iddi.debug) console.log('Setting value of '+e.target.ival.postfieldid);
        if (typeof e.target.ival != 'undefined'){
            current_edit_event=e;
            var v1=e.target.value;
            switch(e.target.ival.type){
                case 'select':
                case 'select-one':
                case 'text':
                    if(iddi.editframe(e.target).val()!='') v1=iddi.editframe(e.target).val();
                    if(iddi.editframe(e.target).text()!='') v1=iddi.editframe(e.target).text();
                    break;
                case 'html':
                    v1=iddi.editframe(e.target).html();
                    break;
            }
            iddiEditor.mark_changed();
            iddiEditor.hoverAreaOver(e);
            e.target.ival.setValue(v1);
        }
    },
    //keyup events occur on inputs only - even in edit mode - so our keyups on editable elements end up on html in iframe or document in main page - this uses getkeyupsource to
    //find the location of the cursor to change the target of the event before passing it on as nromal to value changed event
    valueChangedFindElement:function(e){
        var target=(ie)?$(e.target).iddiGetEditableParent():iddiEditor.getKeyUpSource();
        if(target!=null){
            e.target=target[0];
            iddiEditor.valueChanged(e);
        }
    },
    switchEditMode:function(e){
        if (iddiEditor.editorenabled){
            if(iddi.uiframe(e.target).iddiIsEditable()){
                switch(iddi.uiframe(e.target).iddiEditableNodeData().type){
                    case 'html':
                        current_edit_event=e;
                        iddiEditor.currenteditnode=iddi.uiframe(e.target).iddiGetEditableParent();
                        iddiEditor.switchOnEditMode();
                        iddiEditor.hoverAreaOver(e);
                        iddi.uiframe('.iddi-tools-for-html').show();
                    break;
                    case 'text':
                        current_edit_event=e;
                        iddiEditor.currenteditnode=iddi.uiframe(e.target).iddiGetEditableParent();
                        iddiEditor.switchOnEditMode();
                        iddiEditor.hoverAreaOver(e);
                        break;
                    case 'image':
                        current_edit_event=e;
                        iddiEditor.currenteditnode=iddi.uiframe(e.target);
                        iddiImageEditor.target=iddi.uiframe(e.target);
                        iddiui.loadPopupTool('image-editor-popup','',iddiImageEditor);
                        iddiEditor.hoverAreaOver(e);
                        break;
                    default:
                    iddiEditor.switchOffEditMode();
                    break;
                }
                switch(e.target.tagName){
                    case 'A':
                        return false;
                        break;
                }
            }else{
                iddiEditor.switchOffEditMode();
                iddi.uiframe('.iddi-tools-for').hide();
            }
        }
        //iddiEditor.currenteditnode=null;
    },
    hoverAreaOver:function(e){
        if (iddiEditor.editorenabled){
            if(iddi.editframe(e.target).iddiIsEditable(true)){
                if(iddi.editframe(e.target)[0].window==iddi.editframe.window){
                    try{
                        var etarget=iddi.editframe(e.target).iddiGetEditableParent(true);
                        if(!etarget[0].ival.showborder && typeof etarget[0].ival.NAME != 'undefined'){
                          //etarget[0].ival.type=etarget[0].ival.TYPE;
                          if(etarget[0].ival.type=='html' || etarget[0].ival.type=='image' || etarget[0].ival.type=='text'){
                            //etarget[0].ival.showborder=true;
                            var margin=62;
                            var border=60;
                            var activeclass='';
                            if (typeof current_edit_event!='undefined'){
                                //if (e==current_edit_event) activeclass='active ';
                            }
                            iddi.uiframe('.iddi-bounder').remove();
                            r=etarget[0].getClientRects();
                            var rtop=r[0].top+50;
                            var rleft=r[0].left;
                            var rright=r[0].right;
                            var rbottom=r[0].bottom+50;
                            for(a=1;a<r.length;a++){
                                if (r[a].top<rtop) rtop=r[a].top;
                                if (r[a].left<rleft) rleft=r[a].left;
                                if (r[a].right>rright) rright=r[a].right;
                                if (r[a].bottom>rbottom) rbottom=r[a].bottom;
                            }
                            for(a=1;a<2;a++){
                                iddi.uiframe('body').append('<div style="display:none;" class="'+activeclass+'iddi-bounder-active border_'+a+'" id="iddib_active_top_'+a+'"></div>');
                                iddi.uiframe('body').append('<div style="display:none;" class="'+activeclass+'iddi-bounder-active border_'+a+'" id="iddib_active_right_'+a+'"></div>');
                                iddi.uiframe('body').append('<div style="display:none;" class="'+activeclass+'iddi-bounder-active border_'+a+'" id="iddib_active_bottom_'+a+'"</div>');
                                iddi.uiframe('body').append('<div style="display:none;" class="'+activeclass+'iddi-bounder-active border_'+a+'" id="iddib_active_left_'+a+'"></div>');
                                t=iddi.uiframe('body')[0].getClientRects();
                                n=iddi.uiframe('#iddib_active_top_'+a);
                                n.css('top',rtop-margin+'px');
                                n.css('left',rleft-(margin-border)+'px');
                                n.css('height',border+'px');
                                n.css('width',rright-rleft+(margin+margin-border-border)+'px');
                                n.css('display','block');

                                n=iddi.uiframe('#iddib_active_right_'+a);
                                n.css('top',rtop-margin+'px');
                                n.css('left',rright+margin-border+'px');
                                n.css('height',rbottom-rtop+margin+margin+'px');
                                n.css('width',border+'px');
                                n.css('display','block');

                                n=iddi.uiframe('#iddib_active_bottom_'+a);
                                n.css('top',rbottom-border+margin+'px');
                                n.css('left',rleft-(margin-border)+'px');
                                n.css('height',border+'px');
                                n.css('width',rright-rleft+(margin+margin-border-border)+'px');
                                n.css('display','block');

                                n=iddi.uiframe('#iddib_active_left_'+a);
                                n.css('top',rtop-margin+'px');
                                n.css('left',rleft-margin+'px');
                                n.css('height',rbottom-rtop+margin+margin+'px');
                                n.css('width',border+'px');
                                n.css('display','block');

                                margin=6
                                border=1;
                            }
                            iddi.uiframe('body').append('<div class="iddi-bounder-active-name" id="iddib_active_title">'+etarget[0].ival.NAME+'</div>');
                            n=iddi.uiframe('#iddib_active_title');
                            n.css('top',rbottom+margin+16+'px');
                            n.css('left',(rleft-margin-margin)+'px');
                            iddi.uiframe('body').append('<div class="iddi-bounder-active-icon" id="iddib_active_icon"></div>');
                            n=iddi.uiframe('#iddib_active_icon');
                            n.css('top',rtop-margin-32+'px');
                            n.css('left',rright-margin-15+'px');
                        }
                        }
                        //n[0].passclickto=iddi.uiframe('#'+v.linkname);
                        //n.click(function(){iddi.uiframe(this)[0].passclickto.focus().click();});
                    }catch(e){
                    }
                }
            }
        }
    },
    hoverAreaOut:function(e){
        if (iddiEditor.editorenabled){
            if($(e.target).iddiIsEditable()){
                if(!$(e.target).iddiGetEditableParent().hasClass('iddi-bounder')){
                    $(e.target).iddiGetEditableParent()[0].ival.showborder=false;
                }
            }
        }
        iddiEditor.hideEditBounder();
        if (typeof current_edit_event!='undefined' && current_edit_event!=null) iddiEditor.hoverAreaOver(current_edit_event);
    },
    hideEditBounder:function(){
        iddi.uiframe('.iddi-bounder-active').remove();
        iddi.uiframe('.iddi-bounder-active-icon').remove();
        iddi.uiframe('.iddi-bounder-active-name').remove();
    },
    switchOnEditMode:function(){
        if (iddiEditor.editorenabled){
            iddi.uiframe('#iddi-tb-back').css('opacity','1');
            iddiEditor.activeeditnode=iddiEditor.currenteditnode;
            iddiEditor.activeeditnode.addClass('iddi-live-edit');
            iddiEditor.enableTools(iddiEditor.activeeditnode);
            //frames[0].document.designMode='on';
            //iddiEditor.currenteditnode[0].contentEditable=true;
            //setTimeout("iddi.uiframe(*).removeAttr('_moz_resizing')",400);
            //setTimeout("iddi.uiframe(*).removeAttr('_moz_abspos')",400);
        }
        document.title='on';
    },
    switchOffEditMode:function(){
        iddi.uiframe('#iddi-stuff .iddi-dockable-toolbar').hide();
        if (iddiEditor.activeeditnode != null){
            iddiEditor.activeeditnode.iddiStuff().value=iddiEditor.activeeditnode.html();
            iddiEditor.activeeditnode.iddiStuff().changed=true;
            iddiEditor.activeeditnode.removeClass('iddi-live-edit');
            //iddiEditor.activeeditnode.change();
            if(!ie) iddiEditor.getSelection().removeAllRanges();
        }
        if (typeof current_edit_event!='undefined' && current_edit_event!=null){
         if (typeof current_edit_event.target!='undefined'){
          //current_edit_event.target.designMode='off';
          //current_edit_event.target.contentEditable=false;
         }
        }
        current_edit_event=null;
        iddiEditor.activeeditnode=null;
        iddiEditor.hideEditBounder();
        //frames[0].document.contentEditable=false;
        //frames[0].document.designMode='off';
        document.title='off';
    },
    enableEditMode:function(e){
        if(!iddi.editframe(this).hasClass('iddi-edit-disabled')){
            iddiEditor.currenteditnode=iddi.editframe(this);
            if (!iddi.editframe(this).hasClass('iddi-live-edit')) iddi.editframe(this).addClass('iddi-live-edit');
        }else{
            iddiEditor.currenteditnode=null;
        }
    },
    enableTools:function(t,p){
        if (iddiEditor.editorenabled){
            //enable relevant toolbars
            if (p==0) iddi.uiframe('#iddi-stuff .iddi-dockable-toolbar').hide();
            iddi.uiframe('#iddi-stuff .iddi-dockable-toolbar').each(function(){
                var i=iddi.uiframe(this).attr('id');
                if (t.hasClass(i)) iddi.uiframe(this).show();
            });
            //if (!t.hasClass('iddi-html-root')) iddiEditor.enableTools(t.parent(),p+1);
        }
    },
    createeditor:function(t){
        etype=t.ival.type;
        switch(etype){
            case 'html':

        }
    },
    reloadMain:function(){
         //iddi.uiframe('#iddi-contain-all').iddiAjax('/iddi/iddi.php?context=currentpage ');
    },
    loadBasePage:function(url){
         iddi.editframe(document).iddiAjax(url,function(){
            iddiEditor.setupAfterAjax();
      });
    },
    newpage:function(){
        formdata={};
        iddi.reset();
        iddiui.showoverlay();
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'));
        frames[0].document.location='/iddi/iddi.php?context=create';

        iddiEditor.createEditor();
    },
    load:function(filename){
        iddiui.showoverlay();
        iddiui.showLoader(iddi.uiframe('#iddi-overlay'));
        top.document.location=filename;
        //iddiEditor.createEditor();
    },
    switchEditModeKeyUp:function(e){
      //Not actually required in IE as IE bounds us within the edit area rather nicely.
      if(window.getSelection){
        if (iddiEditor.editorenabled){
            if (iddiEditor.activeeditnode){
                if (!iddi.editframe(frames[0].window.getSelection().anchorNode).iddiIsEditable()){
                    //Disable the editing - Change this later to bounce the focus back to activeeditnode
                    iddiEditor.switchOffEditMode();
                    iddiEditor.currenteditnode=null;
                    return false;
                }else{
                }
            }
        }
      }
    },
    //keyup events occur on inputs only - even in edit mode - so our keyups on editable elements end up on html in iframe or document in main page - this uses get selection
    //to locate the cursor, then finds the next element up from there. We then use our jQuery plugin to find the next editable element up - this will be where our event fired
    getKeyUpSource:function(){
      if(window.getSelection){
        var nod=frames[0].window.getSelection().anchorNode;
        if(nod){
            return iddi.editframe(nod).iddiGetEditableParent();
        }
      }
      return null;
    },
    getCursorPosition:function(){
        return iddiEditor.getSelection().focusOffset;
    },
    getSelection:function(){
        var userSelection;
        if (window.getSelection) {
          userSelection = frames[0].window.getSelection();
        }
        else if (document.selection) { // should come last; Opera!
          userSelection = frames[0].document.selection.createRange();
        }
        iddiEditor.currentSelection=userSelection;
        return userSelection;
    },
    showEditableAreas:function(){
        if (iddiEditor.editorenabled){
            iddi.uiframe('.iddi-bounder').remove();
            var i=1;
            iddi.uiframe('.iddi-editable').each(function(){
                r=iddi.uiframe(this)[0].getBoundingClientRect();
                iddi.uiframe('body').append('<div class="iddi-bounder" id="iddib_'+i+'"><div> </div></div>');
                t=iddi.uiframe('body')[0].scrollTop;
                n=iddi.uiframe('#iddib_'+i);
                n.css('top',r.top+t+'px');
                n.css('left',r.left+'px');
                n.css('height',r.bottom-r.top+'px');
                n.css('width',r.right-r.left+'px');
                //n.passclickto=iddi.uiframe('#'+v.linkname);
                //n.click(function(){iddi.uiframe(this).passclickto.click();});
                i++
            });
        }
    },
    unShowEditableAreas:function(){
        iddi.uiframe('.iddi-bounder').remove();
    },
    insertImage:function(){
        //Insert the image tag and give it an id, then start the image editor
        //iddiEditor.command(insertimage,'',false);
        iddiEditor.imageid++;
        iddiEditor.command('inserthtml','<img src="" id="image-'+iddiEditor.imageid+'"/>',false);
        iddi.editframe('#image-'+iddiEditor.imageid).iddiMakeEditableImage();
        iddiImageEditor.target=iddi.editframe('#image-'+iddiEditor.imageid);
        iddiui.loadPopupTool('image-editor-popup','',iddiImageEditor);
        iddiEditor.imageleftpos=(iddiEditor.imageleftpos)?false:true;
        iddiImageEditor.target.css('float',(iddiEditor.imageleftpos)?'left':'right');
    },
    updateImage:function(){
        image=iddiImageEditor.value;
        iddi.uiframe('#image-'+iddiEditor.imageid).attr('src',image);
    },
    startEditor:function(){
        //Mark setup as done so we don't come in here again
        iddi.setupdone=true;

        //Add a class to the uiframe so we can access it easily from code even with iframes
        iddi.uiframe('html').addClass('iddi-html-root');

        //Listen for any events
        iddi.uiframe('body').change(iddiEditor.valueChanged);

        //Now load the main toolbar - other tools can also be loaded here if needed
        iddi.uiframe('#iddi-stuff').iddiAjax('/iddi/iddi.php',function(){
            //Prevent mouse downs on the toolbar from switching off edit mode
            iddi.uiframe('.iddi-toolbar').mousedown(function(){ return false; })
            //Allow us to hide/show the toolbar
            iddi.uiframe('.iddi-toolbar h6').toggle(iddiui.hideIddi,iddiui.showIddi);
        },'tool',{source:'iddi-edit',tool:'main-toolbar'});
    },
    createEditor:function(){
        //Disable forms
        iddi.editframe('form').removeAttr('method').removeAttr('action').removeAttr('id');
        //Listeners for switching edit mode on and off
        //iddi.uiframe('iframe').mousedown(iddiEditor.switchEditMode);
        iddi.editframe('html').mousedown(iddiEditor.switchEditMode);
        //iddi.editframe('html').keydown(iddiEditor.switchEditModeKeyUp);
        //iddi.editframe('html').keyup(iddiEditor.switchEditModeKeyUp);
        //iddi.editframe('body').focus(iddiEditor.switchEditModeKeyUp);
        iddi.editframe('body').change(iddiEditor.valueChanged);
        iddi.editframe('html').keyup(iddiEditor.valueChangedFindElement);

        iddi.editframe('select').change(iddiEditor.valueChanged);
        iddi.editframe('input').change(iddiEditor.valueChanged);

        iddi.uiframe('body').change(iddiEditor.valueChanged);


        //Listeners for help info on the edit frame
        iddi.editframe('body').mouseover(iddiHelp.showHoverHelp);
        iddi.editframe('body').mouseout(iddiHelp.cancelHoverHelp);
        iddi.editframe('body').mouseover(iddiEditor.hoverAreaOver);
        iddi.editframe('body').mouseout(iddiEditor.hoverAreaOut);
        iddi.editframe('body').mouseup(iddiEditor.hoverAreaOut);
        iddi.editframe('body').focus(iddiEditor.hoverAreaOver);
    },
    setupAfterAjax:function(){
        wizstep=1;
        iddi.attach();

       // iddi.getformdata();
        iddi.uiframe('.iddi-wizard-steps:not(.jax)').addClass('jax').parent().append('<div class="iddi-wizard-step-container">Loading Wizard</div><div class="popup-buttonbar"><div class="iddi-wizard-next-back"><input type="button" class="iddi-wizard-back" value="Back" title="Go Back to the previous step"/><input type="button" class="iddi-wizard-next" value="Next" title="Move forward to the next step"/><input type="button" class="iddi-wizard-finish" value="Finish" title="Finish using the wizard and close it"/></div></div>')
         .find('li').each(function(){
              iddi.uiframe(this).attr('rel',wizstep).click(iddiWizard.loadWizardStep);
              wizstep++;
         })
        .parent().find('li:first').click()
        .parent().parent().find('.iddi-wizard-finish').click(function(){
            iddi.uiframe(this).parent().parent().parent().find('.iddi-close-button').click();
            if(iddi.virtualfilename==null) iddiui.loadPopupTool('saveas-file-popup');

        })
        .parent().parent().find('.iddi-wizard-next-back').find('input').click(function(){iddi.uiframe(this).parent().parent().parent().find("li[rel='"+iddi.uiframe(this).attr('rel')+"']").click();});
         //iddi.uiframe('input:not(.jax)').addClass('jax').click(inputchanged).keyup(inputchanged);
         iddi.uiframe('a.iddi-editable').attr('href','');

        wizstep=1;
    },
    command:function(command,options,dialog){
        if (typeof options == 'undefined') options=null;
        if (typeof dialog == 'undefined') dialog=false;
        if(document.getSelection){
          iddiEditor.ffcommand(command,options,dialog);
        }else{
          iddiEditor.iecommand(command,options,dialog);
        }
    },
    ffcommand:function(command,options,dialog){
        //options=options.replace('<','').replace('>','');
        switch(command){
            case 'bold':
                iddiEditor.wrapSelection('strong');
                //iddiEditor.getdoc().execCommand('styleWithCSS',dialog,'false');
                //iddiEditor.getdoc().execCommand('bold',dialog,options);
                break;
            case 'formatblock':
                iddiEditor.wrapSelection(options);
                break;
            default:
                //iddiEditor.getdoc().execCommand('styleWithCSS',dialog,'false');
                iddiEditor.getdoc().execCommand(command,dialog,options);
                break;
        }
    },
    iecommand:function(command,options,dialog){
        options=options.replace('<','').replace('>','');
        options='<'+options+'>';
        var sText = iddiEditor.getdoc().selection.createRange();
        switch(command){
            case 'bold':
                //If parent node is a strong -
                //iddiEditor.getSelection().getRangeAt(0).commonAncestorContainer.innerHtml
                //iddiEditor.wrapSelection('strong');
                //iddiEditor.getdoc().execCommand('styleWithCSS',dialog,'false');
                iddiEditor.getSelection();
                iddiEditor.currentSelection.execCommand('bold',dialog,options);
                break;
            case 'createlink':
                //Anchor command dosn't need the tags
                options=options.replace('<','').replace('>','');
                iddiEditor.currentSelection.execCommand(command,dialog,options);
                break;
            default:
                //iddiEditor.getdoc().execCommand('styleWithCSS',dialog,'false');
                iddiEditor.getSelection();
                iddiEditor.currentSelection.execCommand(command,dialog,options);
                break;
        }
    },

    getdoc:function(){
        return frames[0].document;
    },
    wrapSelection:function(nodeName){
        var sel=iddiEditor.getSelection().getRangeAt(0);
        var clonedSelection = sel.cloneContents();
        var div = document.createElement ('div');
        div.appendChild(clonedSelection);

        var newNode = document.createElement(nodeName);
        iddiEditor.getSelection().getRangeAt(0).surroundContents(newNode);



        //Remove existing strong nodes from selection html
        var re = new RegExp('<\/?strong[^>]*>','gi');
        var b=$(div).html().replace(re, '');

        //If our selection is inside a strong
        if(iddi.editframe('newnode').parent(nodeName).length>0){
        }else{
          iddi.editframe('newnode').replaceWith('<'+nodeName+'>'+b+'</'+nodeName+'>');
        }

        iddi.editframe(nodeName+' newnode').each(function(){
          var new_content=$(this).parent(nodeName).html().replace(/<newnode[^>]*>/gi, '</'+nodeName+'>').replace(/<\/newnode>/,'<'+nodeName+'>');
          $(this).parent(nodeName).replaceWith('<'+nodeName+'>'+new_content+'</'+nodeName+'>');
        });

        iddi.editframe(nodeName+':empty').remove();


    }
}

//IddiWizard allows us to use ajax based wizards.
iddiWizard={
    wizstep:1,
    loadWizardStep:function(){
        var me=iddi.uiframe(this);
        var stepnumber=me.attr('rel');
        var stepname=me.parent().parent().attr('id')+'-wizard-step-'+stepnumber;
        if(me.hasClass('generated-step')) stepname=me.parent().parent().attr('id')+'-wizard-step-generated';
        me.parent().find('li').removeClass('iddi-wizard-step-active');
        me.addClass('iddi-wizard-step-active');
        iddiui.showLoader(iddi.uiframe(this).parent().parent().find('.iddi-wizard-step-container'));
        me.parent().parent().find('.iddi-wizard-next-back input').each(function(){ if (iddi.uiframe(this).css('opacity')>0) iddi.uiframe(this).stop().fadeTo('medium',0.5);});
        me.parent().parent().find('.iddi-wizard-step-container').iddiAjax('/iddi/iddi.php',function(){
            iddiEditor.setupAfterAjax();
            me=me.parent().parent().find('.iddi-wizard-step-container');
            iddiui.hideLoader(me);
            me.parent().parent().find('.iddi-wizard-back').attr('rel',parseInt(stepnumber)-1);
            me.parent().parent().find('.iddi-wizard-next').attr('rel',parseInt(stepnumber)+1);
            if (stepnumber==me.parent().find('.iddi-wizard-steps > li:first').attr('rel')) me.parent().parent().find('.iddi-wizard-back').stop().fadeTo('medium',0);
            if (stepnumber!=me.parent().find('.iddi-wizard-steps > li:first').attr('rel')) me.parent().parent().find('.iddi-wizard-back').stop().fadeTo('medium',1);
            if (stepnumber==me.parent().find('.iddi-wizard-steps > li:last').attr('rel')) me.parent().parent().find('.iddi-wizard-next').stop().fadeTo('medium',0);
            if (stepnumber!=me.parent().find('.iddi-wizard-steps > li:last').attr('rel')) me.parent().parent().find('.iddi-wizard-next').stop().fadeTo('medium',1);
            //if (stepnumber!=me.parent().find('.iddi-wizard-steps > li:last').attr('rel')) me.parent().parent().find('.iddi-wizard-finish').stop().fadeTo('medium',0);
            //if (stepnumber==me.parent().find('.iddi-wizard-steps > li:last').attr('rel'))
            me.parent().parent().find('.iddi-wizard-finish').stop().fadeTo('medium',1);
            /*
            iddi.uiframe('.iddi-wizard-steps').iddiAjax('/iddi/iddi.php?ajax=1&context=tool&source=iddi-edit&tool=create-new-popup-tabs',function(){
                wizstep=1;
                iddi.uiframe('.iddi-wizard-steps li').each(function(){
                    iddi.uiframe(this).attr('rel',wizstep).click(iddiWizard.loadWizardStep);
                    if(wizstep==stepnumber) iddi.uiframe(this).addClass('iddi-wizard-step-active');
                    wizstep++;
               })
            });
            */
        },'tool',{source:'iddi-edit',tool:stepname});
    }
}


//Jquery extension that allows part of the form data to be attached to a jquery object
$.fn.iddiStuff=function(v){
    this.changed=false;
    if(typeof this[0] != 'undefined'){
        if(typeof v == 'undefined'){
            return this[0].ival;
        }else{
            this[0].ival=v;
        }
    }

    if(!this.hasClass('jax')){
        this.addClass('jax');
        if(typeof v.links != 'undefined') v.links=[];
        v.links.push(this);
    }

    if(typeof this[0] != 'undefined'){
        switch(this[0].type){
            case 'select':
            case 'select-one':
                if(iddi.debug) console.log('Setting option '+v.toSource());
                this.find("option[value!='"+v.value+"']").removeAttr('selected');
                this.find("option[value='"+v.value+"']").attr('selected','selected');
                break;
            case 'radio':
                jQuery('input[name="'+n+'"][type="radio"][value!="'+v.value+'"]').removeAttr('selected');
                if (this.val()==v.value) this.attr('checked','checked');
                break;
            default:
                this.val(v.value);
                break;
        }
        switch(v.type){
            case 'image':
              var d = new Date();
              var da = d.valueOf();
                this.attr('src',v.value+'?t='+da);
                break;
            case 'date':
                this.addClass('datepicker');
                this.append('<input type="text" id="dptarget_'+this.attr('id')+'"/>');
                this.onClick=$('dptarget_'+this.attr('id')).click();
                $('dptarget_'+this.attr('id')).val(v.value);
                $('.datepicker input').datepicker().parent().removeClass('.datepicker');
                $('.datepicker input').change(function(){
                  $(this).parent()[0].ival.setValue($(this).val());
                });
                break;
            default:
              this.change(iddiEditor.valueChanged);
              break;
        }
    }else{
        this.html(v.value);
    }


    return this;
}

//Jquery extension to see if the node is editable with editMode
$.fn.iddiIsEditable=function(forhover){
    if (typeof forhover == 'undefined') forhover=false;
    if (typeof this[0] != 'undefined'){
      if (typeof this[0].ival != 'undefined'){
        if((this[0].nodeName!='INPUT' && this[0].nodeName!='SELECT') || forhover==true){
          return true;
        }else{
          return false;
        }
      }else{
          if(this[0].nodeName!='#document'){
              return jQuery(this).parent().iddiIsEditable();
          }else{
              return false;
          }
      }
    }
    return false;
}

$.fn.iddiEditableNodeData=function(){
    if (typeof this[0] != 'undefined'){
      if (typeof this[0].ival != 'undefined'){
          return this[0].ival;
      }else{
          if(this[0].nodeName!='#document'){
              return jQuery(this).parent().iddiEditableNodeData();
          }else{
              return false;
          }
      }
    }
    return false;
}


//Jquery extension to see if the node is editable with editMode
$.fn.iddiGetEditableParent=function(forhover){
    if (typeof forhover == 'undefined') forhover=false;
    if (typeof this[0].ival != 'undefined'){
            return jQuery(this);
    }else{
        if(this[0].nodeName!='#document'){
            return jQuery(this).parent().iddiGetEditableParent(forhover);
        }else{
            return null;
        }
    }
}

$.fn.iddiMakeEditableImage=function(){
    if (typeof this[0] != 'undefined'){
        this[0].iddiImageEdit=new iddiEditableImage();
        this[0].iddiImageEdit.lastWidth=this[0].width;
        this[0].iddiImageEdit.lastHeight=this[0].height;
        this[0].iddiImageEdit.lastLeft=this[0].getBoundingClientRect().left;
/*        this.mousemove(function(e){
            iddiImageEditor.generateImage(iddi.uiframe(e.target));
        });
        */
        this.mouseout(function(e){
            iddiImageEditor.generateImage(iddi.editframe(e.target));
        });
    }
}

$.fn.iddiShowLoader=function(){
    iddiui.showLoader(jQuery(this));
    return this;
};

$.fn.iddiHideLoader=function(){
    iddiui.hideLoader(jQuery(this));
    return this;
};

$.fn.iddiBringToFront=function(){
    iddiui.bringToFront(jQuery(this));
    return this;
}


//Jquery extension that runs an ajax request, but automatically adds any field updates for the server to the request
$.fn.iddiAjax=function(url,callback,context,post){
    iddiAjaxCaller(url,callback,this,context,post);
}

function iddiAjaxCaller(url,callback,target,context,post){
   if (typeof callback == 'undefined') callback=null;
   if (typeof target == 'undefined') target=null;
   if (typeof post == 'undefined') post={}

   var updates=iddiCore.getUpdates(false);
   if (iddi.debug) console.log('Running iddi Ajax call to '+url);
    if (updates!='{}'){
        //updates=encodeURIComponent(updates);
        post.fieldupdate=updates;
    }
    post.ajax=1;
    post.context=context;

    var d=document.title;

    $.ajax({
       type: "POST",
       url: url,
       data: post,
       dataType: 'json',
       success: function(data){
            if (iddi.debug) console.log('Ajax Success '+data.toSource());
            iddiAjaxProcessor(data,target);
            //iddi.attach();
            if (callback != null) callback(data);
       },
       error:function (XMLHttpRequest, textStatus, errorThrown) {
            if (iddi.debug) console.log('Error '+textStatus+' : '+XMLHttpRequest.toSource());
            //Ignore 500 errors if we have a response
            if(XMLHttpRequest.status==500 && XMLHttpRequest.responseText!=''){
                data=eval('('+XMLHttpRequest.responseText.toString()+')');
            }else{
                if (textStatus=='parsererror'){
                    //Invalid json, lets take a looksy
                    msg='Communication error with server ('+XMLHttpRequest.responseText+')';
                }else{
                    alert(textStatus+':'+errorThrown);
                }
                var data={errors:[{message:msg,type:'fatal'}],content:'Communication Error'};
            }
            iddiAjaxProcessor(data,target);
            if (callback != null) callback(data);
       }

     });

     //document.title=d;
}

var iddiMessages={
    pos:0,
    current:0,
    t:70,
    next:function(msg){
        msg.pos=iddiMessages.pos++;
        iddiMessages.current++;
    },
    retire:function(oncomplete){
        this.addClass('retiring').animate({'opacity':'0','right':'-100%'},400,function(){
            jQuery(this).remove();
            if(typeof oncomplete != 'undefined') oncomplete();
        });
        iddiMessages.current--;
        if(iddiMessages.current==0){
            //Reset the counter
            //iddiMessages.pos=0;
        }
        iddiMessages.positionMessages(300);
    },
    positionMessages:function(s){
        iddiMessages.t=70;
        iddi.uiframe('.iddi-side-message:not(.retiring)').each(function(){
            iddi.uiframe(this).animate({'top':iddiMessages.t+'px'},s);
            iddiMessages.t+=60;
        });
    },
    hideajaxers:function(oncomplete){
        if (iddi.uiframe('.waitonajax').length==0) oncomplete();
        iddi.uiframe('.waitonajax').retire(oncomplete);
    }
}

$.fn.stripTags = function() {
        return this.replaceWith( this.html().replace(/<\/?[^>]+>/gi, '') );
};


function iddiSideMessage(){
    var pos=0;
    var msgid='';
    var msgheight=60;
    var msgoffset=70;
    var donthide=0;
    var waitonajax=0;
    var display;
    this.show=function(status,message){
        var delay=4000;
        if (status=='Success' || status=='success') delay=8000;
        if (status=='serious') delay=8000;
        iddiMessages.next(this);
        this.msgid='iddi-side-message-'+this.pos;
        var top=iddiMessages.t;
        var html='<div id="'+this.msgid+'" class="iddi-side-message iddi-alert-box iddi-status-'+status+'"" style="left:-100%;top:'+top+'px"><span class="icon iddi-status-'+status+'">&nbsp;</span><span class="msg">'+message+'</span></div>';
        iddi.uiframe('#iddi-stuff').append(html);
        iddiMessages.positionMessages(0);
        iddi.uiframe('#'+this.msgid).css('left',0-70-(iddi.uiframe('#'+this.msgid).width())).animate({'left':'0px'},300).iddiBringToFront();
        this.display=iddi.uiframe('#'+this.msgid);
        if (!this.donthide && !this.waitonajax) setTimeout("iddi.uiframe('#"+this.msgid+"').retire()",delay);
        if(this.waitonajax) iddi.uiframe('#'+msgid).addClass('waitonajax');
    };
    this.retire=function(oncomplete){
        iddi.uiframe('#'+this.msgid).retire(oncomplete);
    };
}

$.fn.retire=iddiMessages.retire;

iddiAjaxProcessor=function(data,target){
    //alert(data.toSource());
    iddiMessages.hideajaxers(function(){
        var msg='';
        var allok=true;
        for(err in data.errors){
            error=data.errors[err];
            //msg+="\n"+error.type+' : '+error.message
            if (error.type=='fatal') allok=false;

            var message=error.message;
            if (error.counter > 1) message+=' ('+error.counter+' times)';

            m=new iddiSideMessage();
            m.show(error.type,message);
        }
        //if (msg!='') alert(msg);
        if (allok){
            for(cmd in data.commands){
                var command=data.commands[cmd];
                var code=command.command+'(';
                var comma='';
                for(paramid in command.params){
                    var param=command.params[paramid];
                    code+=comma+param;
                    comma=',';
                }
                code+=')';
                eval(code);
            }
        }

        if (typeof target != 'undefined' && target!=null){
            if (allok){
                //Our xml from the server includes the tool tag - so we copy each child node onto the target, then remove the tool node
                target.html(data.content);
                target.find('tool > *').each(function(){
                    target.append(iddi.uiframe(this));
                });
                target.find('tool').remove();
            }
        }
    });

}


if(this==top){
    iddi.uiframe=$;
}else{
}

function startiddi(){
    iddi.uiframe=$;
    iddi.attach();
    iddi.getformdata();
    if(this==top){
      iddiEditor.createEditor();
      if(document.all){
        $('body').width=$(document).width()+'px';
        $('body').height=$(document).height()+'px';
        $('body').css('overflow','hidden');
      }
    }
    iddi.editframe('a').attr('target','_top');
    iddi.editframe('embed').attr('wmode','transparent');
    //iddi.editframe('object').append('<param name="wmode" value="transparent"/>');
}

/**
 * http://www.openjs.com/scripts/events/keyboard_shortcuts/
 * Version : 2.01.B
 * By Binny V A
 * License : BSD
 */
shortcut = {
    'all_shortcuts':{},//All the shortcuts are stored in this array
    'add': function(shortcut_combination,callback,opt) {
        //Provide a set of default options
        var default_options = {
            'type':'keydown',
            'propagate':false,
            'disable_in_input':false,
            'target':document,
            'keycode':false
        }
        if(!opt) opt = default_options;
        else {
            for(var dfo in default_options) {
                if(typeof opt[dfo] == 'undefined') opt[dfo] = default_options[dfo];
            }
        }

        var ele = opt.target
        if(typeof opt.target == 'string') ele = document.getElementById(opt.target);
        var ths = this;
        shortcut_combination = shortcut_combination.toLowerCase();

        //The function to be called at keypress
        var func = function(e) {
            e = e || window.event;

            if(opt['disable_in_input']) { //Don't enable shortcut keys in Input, Textarea fields
                var element;
                if(e.target) element=e.target;
                else if(e.srcElement) element=e.srcElement;
                if(element.nodeType==3) element=element.parentNode;

                if(element.tagName == 'INPUT' || element.tagName == 'TEXTAREA') return;
            }

            //Find Which key is pressed
            if (e.keyCode) code = e.keyCode;
            else if (e.which) code = e.which;
            var character = String.fromCharCode(code);

            if(code == 188) character=","; //If the user presses , when the type is onkeydown
            if(code == 190) character="."; //If the user presses , when the type is onkeydown

            var keys = shortcut_combination.split("+");
            //Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
            var kp = 0;

            //Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
            var shift_nums = {
                "`":"~",
                "1":"!",
                "2":"@",
                "3":"#",
                "4":"$",
                "5":"%",
                "6":"^",
                "7":"&",
                "8":"*",
                "9":"(",
                "0":")",
                "-":"_",
                "=":"+",
                ";":":",
                "'":"\"",
                ",":"<",
                ".":">",
                "/":"?",
                "\\":"|"
            }
            //Special Keys - and their codes
            var special_keys = {
                'esc':27,
                'escape':27,
                'tab':9,
                'space':32,
                'return':13,
                'enter':13,
                'backspace':8,

                'scrolllock':145,
                'scroll_lock':145,
                'scroll':145,
                'capslock':20,
                'caps_lock':20,
                'caps':20,
                'numlock':144,
                'num_lock':144,
                'num':144,

                'pause':19,
                'break':19,

                'insert':45,
                'home':36,
                'delete':46,
                'end':35,

                'pageup':33,
                'page_up':33,
                'pu':33,

                'pagedown':34,
                'page_down':34,
                'pd':34,

                'left':37,
                'up':38,
                'right':39,
                'down':40,

                'f1':112,
                'f2':113,
                'f3':114,
                'f4':115,
                'f5':116,
                'f6':117,
                'f7':118,
                'f8':119,
                'f9':120,
                'f10':121,
                'f11':122,
                'f12':123
            }

            var modifiers = {
                shift: { wanted:false, pressed:false},
                ctrl : { wanted:false, pressed:false},
                alt  : { wanted:false, pressed:false},
                meta : { wanted:false, pressed:false}    //Meta is Mac specific
            };

            if(e.ctrlKey)    modifiers.ctrl.pressed = true;
            if(e.shiftKey)    modifiers.shift.pressed = true;
            if(e.altKey)    modifiers.alt.pressed = true;
            if(e.metaKey)   modifiers.meta.pressed = true;

            for(var i=0; k=keys[i],i<keys.length; i++) {
                //Modifiers
                if(k == 'ctrl' || k == 'control') {
                    kp++;
                    modifiers.ctrl.wanted = true;

                } else if(k == 'shift') {
                    kp++;
                    modifiers.shift.wanted = true;

                } else if(k == 'alt') {
                    kp++;
                    modifiers.alt.wanted = true;
                } else if(k == 'meta') {
                    kp++;
                    modifiers.meta.wanted = true;
                } else if(k.length > 1) { //If it is a special key
                    if(special_keys[k] == code) kp++;

                } else if(opt['keycode']) {
                    if(opt['keycode'] == code) kp++;

                } else { //The special keys did not match
                    if(character == k) kp++;
                    else {
                        if(shift_nums[character] && e.shiftKey) { //Stupid Shift key bug created by using lowercase
                            character = shift_nums[character];
                            if(character == k) kp++;
                        }
                    }
                }
            }

            if(kp == keys.length &&
                        modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
                        modifiers.shift.pressed == modifiers.shift.wanted &&
                        modifiers.alt.pressed == modifiers.alt.wanted &&
                        modifiers.meta.pressed == modifiers.meta.wanted) {
                callback(e);

                if(!opt['propagate']) { //Stop the event
                    //e.cancelBubble is supported by IE - this will kill the bubbling process.
                    e.cancelBubble = true;
                    e.returnValue = false;

                    //e.stopPropagation works in Firefox.
                    if (e.stopPropagation) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    return false;
                }
            }
        }
        this.all_shortcuts[shortcut_combination] = {
            'callback':func,
            'target':ele,
            'event': opt['type']
        };
        //Attach the function with the event
        if(ele.addEventListener) ele.addEventListener(opt['type'], func, false);
        else if(ele.attachEvent) ele.attachEvent('on'+opt['type'], func);
        else ele['on'+opt['type']] = func;
    },

    //Remove the shortcut - just specify the shortcut and I will remove the binding
    'remove':function(shortcut_combination) {
        shortcut_combination = shortcut_combination.toLowerCase();
        var binding = this.all_shortcuts[shortcut_combination];
        delete(this.all_shortcuts[shortcut_combination])
        if(!binding) return;
        var type = binding['event'];
        var ele = binding['target'];
        var callback = binding['callback'];

        if(ele.detachEvent) ele.detachEvent('on'+type, callback);
        else if(ele.removeEventListener) ele.removeEventListener(type, callback, false);
        else ele['on'+type] = false;
    }
}
