<?php
    /*---------------------------------------------------------------------------------

    IDDI - The Tiny CMS - By J.Patchett @ TasticMedia

    This is the core of the entire system.

    ---------------------------------------------------------------------------------*/
    function iddi_autoload($class_name) {
        global $classes_loaded;
        if(substr_count($class_name,'_')>0){
            $parts=explode('_',$class_name,2);
            $file=__DIR__.'/core/'.strtolower($parts[0]).'/'.strtolower($class_name).'.class.php';
        }else{
            if(file_exists(__DIR__.'/core/'.strtolower($class_name))){
                $file=__DIR__.'/core/'.strtolower($class_name).'/'.strtolower($class_name).'.class.php';
            }else{
                $file=__DIR__.'/core/'.strtolower($class_name).'.class.php';
            }
        }
        if(file_exists($file)){
            require_once($file);
            $classes_loaded++;
        }else{
            //Try plugins instead
            $file=IDDI_PLUGINS_PATH.'/'.str_replace('iddixmliddi_','',strtolower($class_name)).'.class.php';
            if(file_exists($file)){
                require_once($file);
                $classes_loaded++;
            }
        }
    }
    spl_autoload_register('iddi_autoload');

    require_once('mysql.class.php');

/*
    $d=opendir(IDDI_FILE_PATH.'core/');
    while($f=readdir($d)){
        error_reporting(E_ALL);
        $file=IDDI_FILE_PATH.'core/'.$f;
        if(is_file($file)) require_once($file);
    }
*/
    //Strip slashes from post, get, cookies
    //foreach($_POST as $k=>$v) $_POST[$k]=stripslashes($v);
    foreach($_GET as $k=>$v) $_GET[$k]=stripslashes($v);
    foreach($_COOKIE as $k=>$v) $_COOKIE[$k]=stripslashes($v);

    //Reparse the request vars that will have been destroyed by rewrite
    $pagebits=explode('?',$_SERVER['REQUEST_URI']);
    if(isset($pagebits[1])){
      $pagevars=explode('&',$pagebits[1]);
      foreach($pagevars as $pagevar){
          $varbits=explode('=',$pagevar);
          $varname=$varbits[0];
          if ($varname!=''){
              $_REQUEST[$varname]=$varbits[1];
              $_GET[$varname]=$varbits[1];
          }
      }
    }
    //Remove trailing slash
    if($_SERVER['REQUEST_URI']!='/' && substr($_SERVER['REQUEST_URI'],-1)=='/'){
      header('Location: '.substr($_SERVER['REQUEST_URI'],0,-1));
      die();
    }


    //Restore $_GET - stripped by mod_rewrite
    /*
    $uri=explode('?',$_SERVER['REQUEST_URI']);
    $_SERVER['REQUEST_URI']=$uri[0];
    if($uri[1]){
      $pairs=explode('&',$uri[1]);
      foreach($pairs as $pair){
        $values=explode('=',$pair);
        $_GET[$values[0]]=$values[1];
      }
    }*/

    /**
    * @desc Just does a print_r but wraps in pre tags so it displays straight to the browser
    */
    function print_rp($o)
    {
        echo '<pre>';
        print_r($o);
        echo '</pre>';
    }

    //---- Provide a result if this is the starting point of the call - if this file is included it will behave as a library ----

    define('IDDI_ROOT',IDDI_FILE_PATH.'../');
    define('IDDI_PROJECT',IDDI_FILE_PATH.'../iddi-project/');



    //error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
    set_time_limit(30);
    ini_set('memory_limit','128M');


    $START_TIME=microtime(true);
    //Load plugins
    $plugins=new iddiPlugins();
    if (substr_count($_SERVER['SCRIPT_NAME'],'iddi.php')>0 && substr_count($_SERVER['REQUEST_URI'],'/images')==0) @session_start();
    //Load the config
    $config=iddiconfig::Load();
    //Connect to the database
    iddimysql::startup($config);

iddiDebug::message('START:'.$_SERVER['REQUEST_URI']);

    //check for logged in user and reload
    try{
      if($_SESSION['userid']){
        $user=new iddiEntity();
        $user->loadbyid('user',$_SESSION['userid']);
        $_SESSION['user']=$user;
      }
    }catch(Exception $e){
      //Ignore errors here
    }

    /**
    * @desc User management and login methods
    *       This class handles all internal user management and can also be used as a base class for your own user management systems
    *       The class extends iddiEntity and adds the current user as an object to the session so it can be used in templates if required
    * @package Iddi
    * @subpackage Datasources
    */


    if ( ! function_exists( 'exif_imagetype' ) ) {
        function exif_imagetype ( $filename ) {
            if ( ( list($width, $height, $type, $attr) = getimagesize( $filename ) ) !== false ) {
                return $type;
            }
        return false;
        }
    }

    /*********************************************/
    /* Fonction: ImageCreateFromBMP              */
    /* Author:   DHKold                          */
    /* Contact:  admin@dhkold.com                */
    /* Date:     The 15th of June 2005           */
    /* Version:  2.0B                            */
    /*********************************************/

    function ImageCreateFromBMP($filename)
    {
     //Ouverture du fichier en mode binaire
       if (! $f1 = fopen($filename,"rb")) return FALSE;

     //1 : Chargement des ent�tes FICHIER
       $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
       if ($FILE['file_type'] != 19778) return FALSE;

     //2 : Chargement des ent�tes BMP
       $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
                     '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
                     '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
       $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
       if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
       $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
       $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
       $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
       $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
       $BMP['decal'] = 4-(4*$BMP['decal']);
       if ($BMP['decal'] == 4) $BMP['decal'] = 0;

     //3 : Chargement des couleurs de la palette
      $PALETTE = array();
      if ($BMP['colors'] < 16777216 && $BMP['colors'] != 65536)
      {
        $PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
        #nei file a 16bit manca la palette,
      }

     //4 : Cr�ation de l'image
       $IMG = fread($f1,$BMP['size_bitmap']);
       $VIDE = chr(0);

       $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
       $P = 0;
       $Y = $BMP['height']-1;
       while ($Y >= 0)
       {
        $X=0;
        while ($X < $BMP['width'])
        {
         if ($BMP['bits_per_pixel'] == 24)
            $COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
         elseif ($BMP['bits_per_pixel'] == 16)
         {
            $COLOR = unpack("v",substr($IMG,$P,2));
            $blue  = (($COLOR[1] & 0x001f) << 3) + 7;
            $green = (($COLOR[1] & 0x03e0) >> 2) + 7;
            $red   = (($COLOR[1] & 0xfc00) >> 7) + 7;
            $COLOR[1] = $red * 65536 + $green * 256 + $blue;
         }
         elseif ($BMP['bits_per_pixel'] == 8)
         {
            $COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
         }
         elseif ($BMP['bits_per_pixel'] == 4)
         {
            $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
            if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
         }
         elseif ($BMP['bits_per_pixel'] == 1)
         {
            $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
            if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
            elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
            elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
            elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
            elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
            elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
            elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
            elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
            $COLOR[1] = $PALETTE[$COLOR[1]+1];
         }
         else
            return FALSE;
         imagesetpixel($res,$X,$Y,$COLOR[1]);
         $X++;
         $P += $BMP['bytes_per_pixel'];
        }
        $Y--;
        $P+=$BMP['decal'];
       }

     //Fermeture du fichier
       fclose($f1);

     return $res;
    }
