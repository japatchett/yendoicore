<?php
    /**
    * iddiRequestException Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRequestException extends iddiException{}
   