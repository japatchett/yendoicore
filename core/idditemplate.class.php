<?php
    /**
    * iddiTemplate Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiTemplate extends iddiEvents{
        var $id,$filename,$data,$entities;
        protected $_l,$_cvn;
        const BEFORECOMPILE='BeforeCompile', AFTERCOMPILE='AfterCompile',
              BEFOREOUTPUT='BeforeOutput', AFTEROUTPUT='AfterOutput',
              BEFOREXPATH='BeforeXpath', AFTERXPATH='AfterXpath',
              BEFOREGETVALUENODES='BeforeGetValueNodes', AFTERGETVALUENODES='AfterGetValueNodes', GOTVALUENODE='GotValueNode';

        function idditemplate($filename='',$datasource=null){
            $langfilename=str_replace('templates/','templates/'.iddiRequest::$current->language.'/',$filename);
            $filename=(file_exists($langfilename))?$langfilename:$filename;
            $this->filename=$filename;
            if($filename!='') $this->load(); else $this->blank();
            if($datasource!=null) $this->setDataSource($datasource);
        }
        function load(){
            $this->filename=str_replace('.iddi','.xml',$this->filename);
            if (!file_exists($this->filename))
            {
                $this->filename=str_replace('.xml','.iddi',$this->filename);
                if(!file_exists($this->filename)){
                    //try the resource file
                    $fp=pathinfo($this->filename);
                    try{
                        $r=iddiResource::getResource('template:'.$fp['filename']);
                        $data=$r->getRawData();
                    }catch(Exception $e){
                        $filename=$this->filename;
                        while(!file_exists($filename) && strstr($filename,'/')) {
                            $filenameparts=explode('/',$filename,2);
                            $filename=$filenameparts[1];
                        }
                        if(!file_exists($filename)) throw new iddiException('Template file '.$this->filename.' does not exist','iddi.template.load.filenotfound',$this);
                        $this->filename=$filename;
                    }
                }
            }
//            $data=join('',file($this->filename));
//            $this->loadData($data);

            $this->loadData($data);

            return $this;
        }
        function loadData($data=''){
            $loaddata=true;

            $cname=IDDI_FILE_PATH.'templatecache/'.base64_encode($this->filename).':lgn'.iddiRequest::$current->language.'.tch';
            if(IDDI_USE_TEMPLATE_CACHE_MODE==IDDI_CACHE_MODE_FILESYSTEM && file_exists($cname)){
                if(filemtime($cname)>filemtime($this->filename)){
                    $d=file_get_contents($cname);
                    $this->data=unserialize($d);
                    $loaddata=false;
                }
            }
            if(IDDI_USE_TEMPLATE_CACHE_MODE==IDDI_CACHE_MODE_DATABASE){
              $this->data=iddiMySql::get_template($this->filename);
              if ($this->data!=null) $loaddata=false;
            }
            if ($loaddata){
                if ($data=='') $data=file_get_contents($this->filename);
                $this->data=new iddiXmlDocument($data,$this);
                if(IDDI_USE_TEMPLATE_CACHE_MODE==IDDI_CACHE_MODE_FILESYSTEM){
                    @mkdir(IDDI_FILE_PATH.'templatecache');
                    @chmod(IDDI_FILE_PATH.'templatecache',0777);
                    $stor=serialize($this->data);
                    $fp=fopen($cname,'w');
                    fwrite($fp,$stor);
                    fclose($fp);
                }
                if(IDDI_USE_TEMPLATE_CACHE_MODE==IDDI_CACHE_MODE_DATABASE){
                  iddiMySql::cache_template($this);
                }
            }

            $this->_l=true;
        }
        function blank(){
            $data='<!DOCTYPE html><html xmlns:iddi="http://tasticmedia.co.uk/iddi">
                        <head>
                            <title>New Page</title>
                        </head>
                        <body class="iddi">
                            <div class="iddi-center-message iddi-alert-box" id="iddi-is-new-page"><div class="iddi-information icon"><span>I</span></div><h2>This is a blank webpage</h2><p>This message will be replaced once you have chosen a template to use.</p></div><script type="text/javascript">top.iddiui.loadPopupTool(\'create-new-popup\');</script>
                        </body>
                    </html>';
            $this->data=new iddiXmlDocument($data,$this);
            $this->_l=true;
            return $this;
        }

        /**
        * @desc Compiles the template to the database
        */
        function compile(){
            $e=$this->trigger(self::BEFORECOMPILE);
            if (!$e->cancelled){
                echo '.';
                if (!$this->_l) $this->load();
                iddi::Log('Compiling Template '.$this->filename,0);
                //iddiMySql::buildentitytable($this);
                //Compile the entities
                if ($this->entities) foreach($this->entities as $ent) $ent->compile();
                iddiMySql::savefromarray('systemplates',array('name'=>$this->filename,'title'=>$this->name,'description'=>$this->description));
                //Join the primary entity
                if ($this->entities) foreach($this->entities as $ent){
                    iddiMySql::savefromarray('sysjoin_entity_template',array('entityname'=>iddiMySql::tidyname($ent->getname()),'templatefilename'=>$this->filename));
                }
                //Compile each child node
                foreach($this->data->documentelement->children as $child) $child->compile2($this);
                $this->trigger(self::AFTERCOMPILE);
            }
        }


        /**
        * @desc Add an entity to the template
        */
        function addentity($entity){
            if (!($entity instanceof iddiXmlIddi_Entity))  throw new iddiException('addEntity accepts only entities as input','iddi.template.addEntity.notanentity');
            $this->entities[]=$entity;
        }
        function addEditor(){
            if($_SERVER['REQUEST_URI']!='/uploadimage'){
            //Add the editor javascript if we are in edit mode
            //Add the virtual filename
            $pagevars=array('virtualfilename'=>'text','pagetitle'=>'text','id'=>'number','templatefile'=>'text','entityname'=>'text','parentid'=>'number','language'=>'text','rootlanguageitemid'=>'number','basefilename'=>'text','odr'=>'text');
            $d=$this->data->getDataSource();
            if($d->language=='') $d->language='en-gb';

            if($d->id>0){
              if($d->language!=iddiRequest::$current->language){
                $r=new iddiPage();
                $r->templatefile=$d->templatefile;
                $r->entityname=$d->entityname;
                $r->virtualfilename='/'.iddiRequest::$current->language.$d->basefilename;
                $r->rootlanguageitemid=$d->id;
                //$r->parentid=$d->parentid;
                $r->language=iddiRequest::$current->language;
                $r->save();
                iddiRequest::$current->page=$r;
                iddiRequest::$current->pages=array($r);
                //Reload the new one
                echo "<script>parent.document.location='".$r->virtualfilename."'</script>";
                die();
              }
            }



            foreach($pagevars as $pv=>$type){
                $vf=new iddiPostVar($this->data->getCurrentEntity()->entityname,$pv,$d->$pv,$d->id);
                $vf->type=$type;
                //$vf->entityname=
                $vf->HIDDEN='hidden';
            }
            //Inject javascript information about the form
            $js1="\nvar formdata=".iddiRequest::getform()->json().';';

            $scripts=array('/flot/jquery.flot.js','/flot/jquery.flot.tooltip.min.js','/flot/jquery.flot.spline.js','/flot/jquery.flot.resize.js','/flot/jquery.flot.pie.js');

            $p=$this->data->documentelement->xpath('//head')->first();
            if (!$p)  throw new iddiexception('no head found in template - cant start editor','iddi.template.noheader');
            $j=new iddixmlnode($this->data,'script',$p,array('src'=>'http://code.jquery.com/jquery-1.11.1.min.js','type'=>'text/javascript'));
            $j2=new iddixmlnode($this->data,'script',$p,array('type'=>'text/javascript'),$js1);
            $j2=new iddixmlnode($this->data,'script',$p,array('src'=>IDDI_INSTALL_PATH.'jquery/jquery-ui.min.js','type'=>'text/javascript'));
            $j2=new iddixmlnode($this->data,'script',$p,array('src'=>IDDI_INSTALL_PATH.'iddiedit.js?v=1','type'=>'text/javascript'));
            foreach($scripts as $script){
                $j2=new iddixmlnode($this->data,'script',$p,array('src'=>IDDI_INSTALL_PATH.$script,'type'=>'text/javascript'));
            }
            //$j2=new iddixmlnode($this->data,'script',$p,array('src'=>IDDI_INSTALL_PATH.'jquery/jquery.corner.js','type'=>'text/javascript'));
            $j2=new iddixmlnode($this->data,'link',$p,array('href'=>'//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css','rel'=>'stylesheet'));
            //$j2=new iddixmlnode($this->data,'link',$p,array('href'=>IDDI_INSTALL_PATH.'iddi.css','rel'=>'stylesheet'));
            }
        }

        function output($datasource=null,$clean=false,$minify=true){
            $e=$this->trigger(self::BEFOREOUTPUT);
            if (!$e->cancelled){
                if (!$this->_l) $this->load();
                if ($datasource) $this->data->setDataSource($datasource);
                if (iddiRequest::getMode()=='edit') $this->addEditor();
                $output=$this->data->output($datasource,$clean);
                //if (!$clean) $output=str_replace("\n",'',$output);
                $this->trigger(self::AFTEROUTPUT);
                if ($minify){
                    $output=preg_replace('/\n\s+\n/s',"\n",$output);
                    $output=str_replace("\r",'',$output);
                    for($a=0;$a<20;++$a) $output=str_replace('  ',' ',$output);
                    for($a=0;$a<10;++$a) $output=str_replace("\t",' ',$output);
                }
                $output=str_replace('checked="false"','',$output);
                return trim($output);
            }
        }
        function setDataSource($d){
            $this->data->setDataSource($d);
        }
        function xpath($xpath,$includeiddinamespace=false,$resultset=null) { return $this->data->xpath($xpath,$includeiddinamespace,$resultset); }
        function getvaluenodes(){
            if ($this->_l) $this->load();
            if (!isset($this->_cvn)) {
                if ($this->data->allnodes) {
                   foreach($this->data->allnodes as $node) {
                       if ($node instanceof iddixmliddi_value) {
                           if ($node->attributes['TYPE']!='') {
                               $name=strtolower(preg_replace('/[^A-Za-z0-9\-_]/','',$node->attributes['NAME']));
                               $this->_cvn[$name]=$node;
                           }
                       }
                   }
                }
            }
            return $this->_cvn;
        }

    }
