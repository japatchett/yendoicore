<?php
    /**
    * iddiCoreEvents Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiCoreEvents extends iddiEvents{
        const BEFORE_SAVE_ENTITY='Before save entity',AFTER_SAVE_ENTITY='After save entity';
    }

    //---- Utilities ----
   