<?php

class iddiComponent_Flot_Chart{

    function output(){
        //var data1 = [[0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]];

        $sql='SELECT created,sum(total_price) v FROM iddi_order where balance_to_pay=0 and total_price>0 and created>date_sub(now(),interval 30 day) group by created';
        $rs=iddiMySql::query($sql);

        for($a=0;$a<31;$a++){
            $data[date('Y-m-d',strtotime('-'.$a.' days'))]=0;
        }

        foreach($rs as $row){
            $data[date('Y-m-d',strtotime($row->created))]=$row->v;
        }

        $data_string='[';
        $i=0;
        $comma='';
        foreach($data as $item=>$value){
            $data_string.=$comma.'['.$i++.','.$value.']';
            $comma=',';
        }
        $data_string.=']';
        $out.='<script>var salesdata='.$data_string.'</script>';
        $out.='<div id="flotchart" style="margin:20px;height:200px;"></div><script>'.file_get_contents(__DIR__.'/flot_demo.js').'</script>';

        return $out;
    }

}