<?php

class iddiComponent_Flot_Pie{

    function output(){

    $colors=array('#d3d3d3','#bababa','#ED5565','#79d2c0','#1ab394','#23c6c8','#f8ac59');

        $sql='SELECT pagetitle,SUM(price_at_time) v
                FROM iddi_orderitem i
                INNER JOIN iddi_sysfilenames p ON p.id=i.item_id
                INNER JOIN iddi_order o ON o.id=i.order_id
                WHERE balance_to_pay<=0 AND total_price>0
                GROUP BY pagetitle ';
        $rs=iddiMySql::query($sql);

        $running_total=0;
        $other_total=0;

        foreach($rs as $row){
            $overall_total+=$row->v;
        }

        foreach($rs as $row){

            if($row->v/$overall_total>0.1){

                $color=array_shift($colors);
                array_push($colors, $color);

                $dataitem=new stdClass();
                $dataitem->color=$color;
                $dataitem->label=$row->pagetitle;
                $dataitem->data=$row->v;

                $data[]=$dataitem;
            }else{
                $other_total+=$row->v;
            }
            $running_total+=$row->v;
        }

        $dataitem=new stdClass();
        $dataitem->color=$color;
        $dataitem->label='Other';
        $dataitem->data=$other_total;

        $data[]=$dataitem;

        $data_string=json_encode($data);

        $out.='<script>var data='.$data_string.'</script>';
        $out.='<div id="flot-pie-chart" style="margin:20px;height:200px;"></div><script>'.file_get_contents(__DIR__.'/flot_pie.js').'</script>';

        return $out;
    }

}