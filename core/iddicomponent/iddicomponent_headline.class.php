<?php

class iddiComponent_Headline{
    var $title;
    var $subtitle;
    var $figure;
    var $velocity;
    var $plots=array();
    var $graphtype='line';


    function output(){
        $out.='<div class="iddi-dashboard-component iddi-component-headline">';
        $out.='<h1>'.$this->title.'</h1>';
        $out.='<p class="iddi-headline-figure">'.$this->figure.'</p>';
        $out.='<p class="iddi-headline-subtitle">'.$this->subtitle.'</p>';
        if($this->velocity!==null){
            if($this->velocity>1){
                $out.='<p class="iddi-velocity-figure iddi-velocity-up">'.$this->velocity.'% <i class="fa fa-level-up"></i></p>';
            }elseif($this->velocity<1){
                $out.='<p class="iddi-velocity-figure iddi-velocity-down">'.$this->velocity.'% <i class="fa fa-level-down"></i></p>';
            }else{
                $out.='<p class="iddi-velocity-figure iddi-velocity-steady">'.$this->velocity.'% <i class="fa fa-circle-o"></i></p>';
            }
        }
        $out.='</div>';
        return $out;
    }

}