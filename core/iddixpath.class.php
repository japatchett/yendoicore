<?php
    /**
    * iddiXpath Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXpath{ var $axis,$nodetest,$nodepredicates,$nextbit,$functionname,$functionparams;
      /**
      * @desc Prepares an xpath statement for use, removing any shortcuts
      */
      static function prepare($xpath){
            if (substr($xpath,0,2)=='//') $xpath='/descendant-or-self::'.substr($xpath,2);
            $xpath=str_replace('..','parent::node()',$xpath);
            $xpath=str_replace('/./','/self::node()/',$xpath);
            $xpath=str_replace('./','self::node()/',$xpath);
            $xpath=str_replace('/.','/self::node()',$xpath);
            $xpath=str_replace('//','/descendant-or-self::node()/',$xpath);
            $xpath=str_replace('@','attribute::',$xpath);
            return $xpath;
      }

      function iddiXpath($xpath)
      {
            $this->fullxpath=$xpath;
            preg_match('/([^:\[\/]*)(::)*([^\[\/]*)(\[(.*?)\])*(\/)*(.*)/',$xpath,$matches);

            $this->axis=($matches[2]=='::')?$matches[1]:'child';
            $this->nodetest=($matches[2]=='::')?$matches[3]:$matches[1];
            $this->nodepredicates=$matches[5];
            $this->nextbit=$matches[7];

            //Are we making a function call?
            if (substr_count($this->nodetest,'(')>0){
                preg_match('/([^\(]*)\(([^\)]*)\)/',$this->nodetest,$matches);
                //preg_match("/([^\(]*)\((.*?)\)/",$this->nodetest,$matches);
                $this->functionname=$matches[1];
                $this->functionparams=$matches[2];
                $this->classfunctionname='xmlFunction_'.$this->functionname;
            }
      }
      function __toString(){return $this->fullxpath;}
    }


   