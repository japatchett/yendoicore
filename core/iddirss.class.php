<?php
    /**
    * iddiRss Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRss extends iddiEvents{
        const EVT_DOWNLOAD_FEED='EVT_DOWNLOAD_FEED';
        var $localfilename;

        static function getFeed($url,$target){
            $f=new iddiRss();
            $f->downloadToCacheFile($url,$target);
        }

        function downloadToCacheFile($url,$target){
            $data=file_get_contents($url);
            if (strlen($data)>2){
                if (strstr($data,'<?xml')){
                    //Write to the cache
                    $fp=fopen($target,'w');
                    fwrite($fp,$data);
                    fclose($fp);
                    //Remove the async flag file
                    @unlink($target.'.async');

                }
            }
            $this->localfilename=$target;
            if ($this->hasObservers()) $this->trigger(self::EVT_DOWNLOAD_FEED);
            //Return the data
            return $data;
        }
        /**
        * @desc Request a new rss feed without holding up the current request - The RSS data will not be available to this request or until downloaded
        */
        static function getFeedAsync($url,$target){
            //Write a flag file to prevent other asyncs on this url starting
            $fp=fopen($target.'.async','w');
            fwrite($fp,'a');
            fclose($fp);
            $url=urlencode($url);
            $target=urlencode($target);
            //Open a connection and then close it straight away
            $fp=fopen('http://'.$_SERVER['HTTP_HOST']."?context=offline_rss&url={$url}&target={$target}",'r');
            fclose($fp);
        }
    }

    //---- Templating System -----
