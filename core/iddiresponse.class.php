<?php
    /**
    * iddiResponse Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiResponse extends iddiBase{ var $template; static $form;
        var $savetocache=false,$usecache=false;
        protected $_ajax,$_request;
        var $content;
        function __construct(iddiRequest $request=null){
            if ($request==null) $request=iddiRequest::$current;
            $this->_request=$request;
            $this->_ajax=new iddiAjaxReponse();
        }
        function addError($message,$code='iddi.ajax.unknown',$type='warning'){
          if(is_a($message,'iddiException')) $message=$message->toErrorMessage();
            $this->_ajax->addError($message,$code,$type);
        }
        function addObject($object){
            $this->_ajax->addObject($object);
        }
        function addStat($stat,$value){
            $this->_ajax->addStat($stat,$value);
        }
        function addCommand($command,$params){
            $this->_ajax->addCommand($command,$params);
        }
        function getJson(){
            $this->_ajax->content=$this->content;
            return json_encode($this->_ajax);
        }
        function locatepage($url){

        }
        function render() {
            $cf='cache'.$_SERVER['REQUEST_URI'].'.sto';
            if ($this->usecache && file_exists($cf)){
                $result=file_get_contents($cf);
            }else{
                if ($this->_request->isajax){
                    if ($this->content=='' && $this->template) $this->content=$this->template->output();
                    $time=microtime()-$this->_request->starttime;
                    $mem=memory_get_usage(true)-$this->memory;
                    $this->addStat('memoryusage',$mem);
                    $this->addStat('runtime',$time);
                    $result=$this->getJson();
                    header("Content-type: application/json");
                }else{
                    if (!$this->template) return ''; //  throw new iddiException('No Template Set','iddi.response.render.notemplate');
                    $data='<!DOCTYPE '.iddiXmlDocument::$doctype.'>'."\n".$this->template->output();

                    //$data='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n".$this->template->output();
                    //$data=str_replace('<html','<html xmlns="http://www.w3.org/1999/xhtml"',$data);

                    //$data=str_replace('&','&amp;',$data);
                    //$data=str_replace('&amp;amp;','&',$data);

                    $data=str_replace('<pagetitle class="first"></pagetitle>','',$data);
                    $data=str_replace('<pagetitle class="active expanded"></pagetitle>','',$data);
                    $data=str_replace('<pagetitle></pagetitle>','',$data);

                    $data=str_replace('&amp;curlyopen;','%7B',$data);
                    $data=str_replace('&amp;curlyclose;','%7D',$data);
                    $data=str_replace('&curlyopen;','%7B',$data);
                    $data=str_replace('&curlyclose;','%7D',$data);

                    if ($this->savetocache){
                        $p=pathinfo($cf);
                        $pf='';
                        $t=explode('/',$cf);
                        foreach($t as $t1){
                            if (substr($t1,-4)!='.sto'){
                                $pf.=$t1.'/';
                                @mkdir($pf);
                                @chmod($pf,0777);
                            }
                        }
                        $fp=fopen($cf,'w');
                        fwrite($fp,$data);
                        fclose($fp);
                    }
                    $result=$data;
                }
            }
            //For the debugger
            if (iddi::$debug){
                iddiDebug::dumpvar('Response',$this);
                iddiDebug::dumpvar('Output',$result);
            }
            if(IDDI_FULL_CACHE_PATH){
              @mkdir(IDDI_FULL_CACHE_PATH);
              @chmod(IDDI_FULL_CACHE_PATH,0777);
              $bits=explode('/',$_SERVER['REQUEST_URI']);
              $path=IDDI_FULL_CACHE_PATH;
              foreach($bits as $bit){
                $path.=$bit.'/';
                @mkdir($path);
                @chmod($path,0777);
              }
              $filename=$path.'index.html';
              $fp=@fopen($filename,'w');
              if($fp){
                @fwrite($fp,$result);
                @fclose($fp);
              }
            }
            return $result;
        }
        static function getform(){
        if (!self::$form) self::$form=new iddiform();
          return self::$form;
        }
    }
