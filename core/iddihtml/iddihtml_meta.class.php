<?php

/**
 * iddiHtml_Meta class.
 *
 * Just sets meta tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Meta extends iddiHtml_Void {}
