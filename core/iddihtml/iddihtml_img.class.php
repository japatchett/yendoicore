<?php
/**
 * iddiHtml_Img class.
 *
 * Sets img tags as void and self closing (depending on doctype) and also ensures
 * that they have an alt tag so that it validates
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Img extends iddiHtml_Void {

    function output($clean = false, $level = 0) {
        /**
         * Ensure that we always have an alt tag so output is valid
         */
        if (!$this->attributes['ALT']) $this->setAttribute('alt', $this->getDataSource()->pagetitle);

        $this->attributes['SRC']=str_replace('{','%7B',str_replace('}','%7D',($this->attributes['SRC'])));

        return parent::output();
    }

}
