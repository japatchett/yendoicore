<?php
    /**
    * iddiImageField Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiImageField{
        function createForEditor(){
            //This will generate a new image file and return the filename
            $fieldData=unserialize(urldecode($_REQUEST['instructions']));
            $value=$_REQUEST['src'];
            $v=parse_url($value);
            $value=$v['path'];
            $filename=$this->parseValue($value,$fieldData);
            $filename=str_replace('/iddi-project','',$filename);
             iddiDebug::message('Returning filename '.$filename);


            return $filename;
        }
        function parseValue($value,$fieldData){
          try{
            //We will check for the correct image in the cache and generate it if it dosn't exist, then return a link to the new filename
            //Incoming filename will be from the library
            $v=parse_url($value);
            $value=$v['path'];

            $if=pathinfo($value);

            $value=urldecode($value);
            $value=str_replace('&curlyopen;','{',$value);
            $value=str_replace('&curlyclose;','}',$value);
            $value=preg_replace('/\{[^\}]*\}/','',$value);

            iddiDebug::dumpvar('Processing Image '.$value,$value);

            if(substr_count($value,'images/')>0){

              //Find the dir above /images/ and use that as our basis for the filename
              $filenameparts=explode('/images/',$value);
              $filenameparts=explode('/',$filenameparts[1],2);
              $sourcefile=$filenameparts[1];
            }else{
              $sourcefile=$value;
            }

            //Work out where the original is, and the target
            $inputfilename=IDDI_IMAGES_PATH.'/originals/'.$sourcefile;
            $outputfilename=IDDI_IMAGES_PATH.'/public/'.$sourcefile;

            iddiDebug::message("Processing source $inputfilename to $outputfilename");

            $of=pathinfo($outputfilename);
            $outputfilename=$of['dirname'].'/'.$of['filename'];
            //Add the processing instructions to the output filename
            $pi='_';
            foreach($fieldData as $nodeid=>$instruction){
                foreach($instruction as $instructionname=>$params){
                    $pi.=$instructionname.':';
                    foreach($params as $attribute=>$avalue){
                        $pi.=$attribute.':'.$avalue.':';
                    }
                }
            }
            $pi=base64_encode($pi);
            $outputfilename.='{'.$pi.'}';
            $outputfilename.='.'.$if['extension'];


            if (!file_exists($outputfilename)){
                //Dosn't exist, so make it
                $image=new iddiImage($inputfilename);
                foreach($fieldData as $nodeid=>$instruction){
                    foreach($instruction as $instructionname=>$params){
                        switch($instructionname){
                            case 'resize':
                                    $image->resize($params['height'],$params['width'],$params['scalemode'],($params['upscale'])?$params['upscale']:true);
                                break;
                        }
                    }
                }
                $image->save($outputfilename);
            }
          } catch(Exception $e){
            if(iddi::$debug) iddiDebug::dumpexception('Problem processing image',$e);
          }
            $outputfilename=str_replace('../','',$outputfilename);
            if (substr($outputfilename,0,1)!='/') $outputfilename='/'.$outputfilename;

            return $outputfilename;
        }
    }
