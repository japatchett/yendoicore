<?php
    /**
    * iddiXmlIddiEditableNode Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddiEditableNode extends iddiXmlIddiNode{
        function wrapIfNecessary(){
            //Basically if our parent node has any other descendants we need to wrap our edit node in a div. If designers need to override this to get a good
            //WYSIWYG output do it manually adding an editmodeonly="true" attribute if you don't want the extra tag to ruin nice markup
            if (count($this->parent->children)>1){
                $div=new iddiXmlNode($this->owner,'div',$this->parent);
                $div->appendChild($div);
            }
        }
    }
   