<?php
    /**
    * iddiException Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiException extends Exception{ var $code; var $innerexception; var $sourceobject;
      function __construct($message,$code='iddi.unknown',$sourceobject=null,$innerexception=null){
          $this->code=$code;
          $this->innerexception=$innerexception;
          $this->sourceobject=$sourceobject;
          parent::__construct($message);

          if(iddi::$debug) iddiDebug::dumpexception('EXCEPTION:'.$message,$this);

      }
      function __toString()
      {
          $output.= '<div style="font-family:arial;padding:20px;margin:20px;border:solid 1px black;background:#fffafa;">';
          $errorfile=IDDI_DOCS_PATH.'developer/errors/'.$this->code.'.html';
          $d=file_get_contents($errorfile);
          if ($d==''){
            if(file_exists(IDDI_FILE_PATH.'docs')){
              $errorfile=IDDI_FILE_PATH.'docs/developer/errors/'.$this->code.'.html';
              $d='<h1>'.$this->code."</h1>
<p>{MESSAGE}</p>
<h2>Cause</h2>
<p>No information yet.</p>
<h2>Fix</h2>
<p>There is no information about how to fix this problem yet.</p>";
              $fp=fopen($errorfile,'w');
              fwrite($fp,$d);
              fclose($fp);
            }
          }
          $d=str_replace("{MESSAGE}",$this->message,$d);
          $output.= $d;
          //if(iddiRequest::$current->devmode){
              //if ($this->sourceobject && method_exists($this->sourceobject,'dump')) $this->sourceobject->dump();
              $output.= '<h2>Stack Trace</h2><p>Most recently called line first.</p><pre>'.parent::__toString().'</pre>';
              $output.= '</div>';
          //}
          return $output;
      }
      function toErrorMessage(){
        return get_class($this).' : '.$this->message.' ('.$this->code.')<br/>'.str_replace("\n","<br/>",parent::__toString());
      }
    }
    /**
    * @desc Coding exceptions can be handled by code, template or request
    */
