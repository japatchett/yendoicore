<?php
    /**
    * iddiXpathResultSet Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXpathResultSet extends iddiIterator{
      function addResult($node){
          $this->items[]=$node;
      }
      function addResults(iddiXpathResultSet $results){
          foreach($results->getResults() as $r) $this->addResult($r);
      }
      function getResults(){ return $this->items; }
      static function quickResult($name,$value){
            $r=new iddiXpathResultSet();
            $x=new iddiXmlNode();
            $x->nodename=$name;
            $x->value=$value;
            $r->addResult($x);
            return $r;
      }
    }
   