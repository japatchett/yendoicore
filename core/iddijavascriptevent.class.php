<?php
    /**
    * iddiJavascriptEvent Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiJavascriptEvent{
        var $eventname,$params;
        function iddiJavascriptEvent($eventname,$params){
            $this->eventname=$eventname;
            $this->params=$params;
            $_SESSION['events'][]=$this;
            if (sizeof($_SESSION['events'])>40) array_pop($_SESSION['events']);
        }
        static function getEvents(){
            $j=json_encode($_SESSION['events']);
            unset($_SESSION['events']);
            return $j;
        }
        static function getEventsScript(){
            $e=self::getEvents();
            return '<script type="text/javascript">processeventdata('.$e.',\''.$e.'\');</script>';
        }

    }
   