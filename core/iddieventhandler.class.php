<?php
    /**
    * iddiEventHandler Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiEventHandler { var $o; var $m;
      function iddiEventHandler($to,$method){ $this->o=$to;  $this->m=$method; }
      function trigger($e,$n='unknown') {
          $o=$this->o;
          $m=$this->m;
          if (is_object($o)){
              if(iddi::$debug) iddiDebug::dumpvar('Triggering event '.get_class($e->sourceobject)."->{$n} to ".get_class($o)."->{$m}",$this);
              $o->$m($e);
          }else{
              if(iddi::$debug) iddiDebug::dumpvar('Triggering event '.get_class($e->sourceobject)."::{$n} to ".get_class($o)."::{$m} (static)",$this);
              eval('$o::$m($e)');
          }
      }
    }
   