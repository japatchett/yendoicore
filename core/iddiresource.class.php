<?php
    /**
    * iddiResource Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiResource{
        private $getname,$inresource=false,$result,$language='en-gb',$finished=false,$found=false;

        /**
        * @desc Generates a new resource object that can be rendered or manipulated
        * @param string $inResourceName Name of the resource to fetch from the resource file - eg logonpage
        * @param string $inLanguage The language we want - uses the system language if null or blank
        * @return iddiResource Returns the new resource object
        */
        static function getResource($inResourceName,$inLanguage='en-gb'){
            //Bounds Checking
            if (!is_string($inResourceName)) throw new iddiException('Resource name invalid','iddi.resource.staticGetResource.invalidResourceName',$this);

            $out=new iddiResource();
            $out->getname=$inResourceName;
            $out->language=$inLanguage;
            $out->process();
            return $out;
        }

        /**
        * @desc Returns the raw data - same as $object->result
        */
        function getRawData(){
            return $this->result;
        }

        /**
        * @desc Physically renders the output and stops processing.
        */
        function render(){
            $r=new iddiResponse();
            $r->template=new iddiTemplate();
            $r->template->loadData($this->result);
            echo $r->render();
            die();
        }

        /**
        * @desc Processes the loaded resource
        */
        private function process(){
            //Bounds Checking
            if (!$this->getname) throw new iddiException('No resource set','iddi.resource.noResourceSet',$this);
            if (!$this->language) throw new iddiException('No language set','iddi.resource.noLanguageSet',$this);
            //Function to start the parsing once all values are set and
            //the file has been opened
            $parser_object = xml_parser_create();
            xml_set_object($parser_object, $this);

            //Set up the parse object - we're using a custom one here for speed purposes
            xml_parser_set_option($parser_object, XML_OPTION_CASE_FOLDING, false);
            xml_set_element_handler($parser_object,'startElement','endElement');
            xml_set_character_data_handler($parser_object, 'contentHandler');

            //Reset this->found so we can test for successfully finding the required resource
            $this->found=false;
            $this->finished=false;
            $this->result='';

            //Build the filename and check it exists locallly
            $resourcefile=IDDI_FILE_PATH.'languages/'.$this->language.'.xml';
            if (!file_exists($resourcefile)){
              //If not try the gateway, otherwise fail
              $resourcefile=IDDI_GATEWAY.$this->language.'.xml';
              if (!file_exists($resourcefile)) throw new iddiException('Resource file not found','iddi.resource.resourceFileNotFound',$this);
            }

            //Start reading the file - we parse it as we go and we'll stop when $this->finished is true to save the need to continue parsing the whole file
            if ($fp = fopen($resourcefile, 'r')){
                while ($data = fread($fp, 1024)) {
                    xml_parse($parser_object, $data, feof($fp));
                    if ($this->finished) break;
                }
            }else{
                throw new iddiException('Resource exists but failed to open','iddi.resource.resourceFileFailedToOpen',$this);
            }
            if (!$this->found){
                throw new iddiException('Resource does not exist in resource file','iddi.resource.resourceNotFound',$this);
            }
            return $this->result;
        }

        private function startElement($parser_object, $elementname, $attributes) {
            //Bounds Checking
            if (!$this->getname) throw new iddiException('No resource set','iddi.resource.noResourceSet',$this);
            if (!is_resource($parser_object)) throw new iddiException('Parser is not a valid resource','iddi.resource.startElement.parserIsNotAResource',$this);
            if (!is_string($elementname)) throw new iddiException('Elementname invalid','iddi.resource.startElement.elementNameInvalid',$this);

            if ($this->inresource){
                $this->result.='<'.$elementname;
                foreach($attributes as $aname=>$avalue){
                    $this->result.=" {$aname}=\"{$avalue}\"";
                }
                $this->result.='>';
            }
            if ($elementname == $this->getname){
                $this->inresource=true;
                $this->found=true;
            }
        }
        private function endElement($parser_object, $elementname) {
            //Bounds Checking
            if (!$this->getname) throw new iddiException('No resource set','iddi.resource.noResourceSet',$this);
            if (!is_resource($parser_object)) throw new iddiException('Parser is not an object','iddi.resource.endElement.parserIsNotAResource',$this);

            if ($elementname == $this->getname){
                $this->inresource=false;
                $this->finished=true;
            }
            if ($this->inresource) $this->result.="</{$elementname}>";
        }

        private function contentHandler($parser_object,$data){
            if ($this->inresource) $this->result.=$data;
        }

    }
   