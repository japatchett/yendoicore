<?php
    /**
    * iddiPage Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiPage extends iddiEntity{
      static $tempid=1;
      const BEFOREPAGELOAD='BeforeLoad',AFTERPAGELOAD='AfterLoad',BEFOREOUTPUT='BeforeOutput', AFTEROUTPUTGENERATED='AfterOutputGenerated',BEFORESAVE='BeforeSave5',AFTERSAVE='AfterSave6';
      function __construct(){
          $this->id=0-(++self::$tempid);
          parent::__construct();
      }
      function LoadPage($invirtualfilename){
          $e=$this->trigger(self::BEFOREPAGELOAD);
          if (!$e->cancelled)
          {
              iddiMySql::loadpagebyvf($invirtualfilename,$this);
              $this->currentlanguagecode=iddiRequest::$current->language;
              $this->rawDbData['currentlanguagecode']=iddiRequest::$current->language;
              $this->dbfields['currentlanguagecode']=iddiRequest::$current->language;
              $this->trigger(self::AFTERPAGELOAD);
          }
      }
      function LoadPageId($inEntityName,$inPageId){
          $e=$this->trigger(self::BEFOREPAGELOAD);
          if (!$e->cancelled)
          {
              iddiMySql::loadpagebyid($inEntityName,$inPageId,$this);
              $this->trigger(self::AFTERPAGELOAD);
          }
      }
      function getTemplate(){
          if ($this->templatefile=='')  throw new iddiException('Page has no template','iddi.page.loadPage.notemplate',$this);
          $template=new idditemplate($this->templatefile,$this);
          return $template;
      }
      function setTemplateData($data){

      }

      function output(){
          if (!$this->template)  throw new iddiException('Page has no template','iddi.page.output.notemplate',$this);
          return $this->template->output($this);
      }

      function Save(){
          $e=$this->trigger(self::BEFORESAVE);
          if (!$e->cancelled){

          if($this->created==0) $this->created=date('Y-m-d H:i:s',time());
          //if($this->owner==0) $this->owner=iddiUser::$current_user->id;

          $this->modified=date('Y-m-d H:i:s',time());
          //$this->lastmodifiedby=iddiUser::$current_user->id;

          //Link the widget entities
            //iddiMySql::savePage($this);
            iddiMySql::saveEntity($this);
                      //Save in the virtual filename table
            $this->trigger(self::AFTERSAVE);
          }
    //          if ($this->data->entityname) iddiMySql::savefromarray('sysjoin_entity_template',array('entityname'=>$this->data->getCurrentEntity()->getname(),'templatefilename'=>$this->filename));

      }

        function xmlFunction_current(){
            $r=new iddiXpathResultSet();
            $r->addResult(iddiRequest::getMainRequest()->page);
            return $r;
        }
        function xmlFunction_entityname(){
            $r=new iddiXpathResultSet();
            $x=new iddiXmlNode();
            $x->value=$this->entityname;
            $r->addResult($x);
            if(iddi::$debug) iddiDebug::dumpvar('Running Function entityname on entity. ('.$this->id.') Detail contains entity',$this->dump(2));
            return $r;            
        }
        function xmlFunction_language(){
            $r=new iddiXpathResultSet();
            $x=new iddiXmlNode();
            $x->value=iddiRequest::$current->language;
            $r->addResult($x);
            return $r;
        }
        function xmlFunction_id(){
            $r=new iddiXpathResultSet();
            $x=new iddiXmlNode();
            $x->value=$this->id;
            $r->addResult($x);
            return $r;
        }
        function xmlFunction_request(){
            //$r=new iddiXpathResultSet();
            //$r->addResult(iddiRequest::$current->toXml()->documentelement);
            //return $r;
        }
    }

   