<?php
    /**
    * iddiDataSet Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiDataSet extends iddiDataSource implements Iterator
    {
        protected $_resultset;
        protected $_currentposition=0;
        protected $_currentrow;
        protected $_pagelength=1000;
        protected $_currentpage;
        protected $_stopatendofpages=true;
        protected $_currentrowdata;

        var $id;
        var $keepafteruse=false;
        var $isNew=false,$newId=null;

        public function __construct($in_resultset=null){if ($in_resultset) $this->LoadResultSet($in_resultset);}
        public function LoadResultSet($in_resultset){
            $this->_resultset=$in_resultset;
            $this->rewind();
            if($this->_currentrowdata['entityname']) $this->entityname=$this->_currentrowdata['entityname'];
        }
        public function rewind(){ if($this->_resultset) { if (mysql_num_rows($this->_resultset) > 0) $this->GetRow(0); return $this;}}
        /**
        * @desc Returns the current record - or a field value from the current record if a fieldname is supplied
        * @param string $fieldname Optional field name to return instead of the entire record
        */
        public function current($fieldname=''){
            if ($this->_resultset){
                if ($fieldname!=''){
                    return $this->_currentrow->$fieldname;
                }else{
                    return $this->_currentrow;
                }
            }
        }
        public function key(){if ($this->_currentrow) return $this->_currentrow->id;}
        public function next(){if ($this->_resultset) return $this->GetRow();}
        public function valid(){if ($this->_resultset) return $this->current() !== null;}
        public function count(){return ($this->_resultset)?mysql_num_rows($this->_resultset):0;}
        public function item($id){return $this->GetColumn($id);}
        public function set_page_length($length){$this->_pagelength=$length;}
        public function has_data(){return $this->count() > 0;}
        public function get_first_row(){return $this->get_row(0);}
        public function jump_to_page($page){
            $this->_currentpage=$page-1;
            $this->rewind();
        }
        public function next_page(){
            $this->_currentpage++;
            $this->rewind();
            return $this;
        }
        public function previous_page(){
            if ($this->_currentpage > 0){
                $this->_currentpage--;
                $this->rewind();
                return $this;
            }else{
                return false;
            }
        }
        public function first_page(){
            $this->_currentpage=0;
            $this->rewind();
        }
        public function get_pos(){ return $this->_currentposition; }
        public function get_page(){ return $this->_currentpage; }
        private function get_row($gcolnumber=NULL){
            if (is_numeric($gcolnumber)){
                $gcolnumber=($this->_currentpage * $this->_pagelength) + $gcolnumber;
                mysql_data_seek($this->_resultset,$gcolnumber);
                $this->_currentposition=$gcolnumber;
            }

            if ($this->_currentposition <= $this->_pagelength || $this->_stopatendofpages==false){
                $this->_currentrowdata=mysql_fetch_assoc($this->_resultset);
                if ($this->_currentrowdata){
                    $this->_currentrow=$this->toEntity();
                    $this->_currentposition++;
                }else{
                    $this->_currentrow=null;
                }
            }else{
                $this->_currentrow=null;
            }
            return $this->_currentrow;
        }
        function toEntity(){
            $entity=($this->entity)?$this->entity:'iddiMySqlRow';
            if (!class_exists($entity)) $entity='iddiMySqlRow';
            $e=new $entity;
            $this->populateEntity($e);
            $e->recordset=$this;
            return $e;
        }
        /**
        * @desc Populates the provided entity with the contents of the current record
        * @param iddiDataSource $entity The entity to populate
        * @param bool $buildentityvars Set to true to build the entity variables
        * @param bool $mergevalues Set to true to merge the values with those already in the entity - i.e. do not overwrite existing values in the entity
        */
        function populateEntity($entity,$buildentityvars=false,$mergevalues=false){
            if($this->_currentrowdata) foreach($this->_currentrowdata as $k=>$v){
                $k=strtolower($k);
                $v=str_replace("&gt;",">",$v);
                $v=str_replace("&lt;","<",$v);
                $v=str_replace("&newline;","\n",$v);
                $v=stripslashes($v);

                //$v=str_replace("%u","&#",$v);
                if($k[0]!='_'){
                  if($v!='' || $mergevalues==false){
                      $entity->setDbValue($k,$v,$buildentityvars);
                      $entity->rawDbData[$k]=$v;
                  }
                }
            }
        }

        function populateObject($object){
            if($this->_currentrowdata) foreach($this->_currentrowdata as $k=>$v){
                $k=iddiMySql::tidyname($k);
                $k=strtolower($k);
                $v=str_replace("&gt;",">",$v);
                $v=str_replace("&lt;","<",$v);
                $v=str_replace("&newline;","\n",$v);
                //$v=str_replace("%u","&#",$v);
                $v=stripslashes(urldecode($v));
                $object->$k=$v;
            }
        }
        function dumpTable(){
            $r=$this->getFirstRow();
            $output='<style>td {valign:top;max-height:90px;overflow:scroll;}</style><table border="1"><tr><th>#</th>';

            foreach($this->_currentrowdata as $k=>$v){
                $output.="<th>$k</th>";
            }
            $output.="</tr>";
            $p=1;
            foreach($this as $row){
                $output.="<tr><td>$p</td>";
                foreach($this->_currentrowdata as $k=>$v){
                    $output.="<td>$v</td>";
                }
                $output.="</tr>";
                ++$p;
            }
            $output.="</table>";
            $this->rewind();
            return $output;
        }
    }

    //---- Custom Node Handlers ----
