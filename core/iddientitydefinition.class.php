<?php
    /**
    * iddiEntityDefinition Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiEntityDefinition{
          var $fielddefs,$entityname;
          static $entities;
          function __construct($inEntityName){
              if ($inEntityName=='') throw new iddiCodingException('No Entity Name Provided','iddi.core.iddiEntityDefinition.Constructor.NoEntityId');
              $this->entityname=$inEntityName;
              $this->fielddefs=iddiMySql::getEntityDefinition($this->entityname);
              $d=iddiMySql::getEntityInfo($this->entityname);
              foreach($d->fields as $k=>$v) $this->$k=$v;
          }
          /**
          * @desc Gets the full definition for the given field
          */
          function GetFieldDefinition($inField){
              if(!isset($this->fielddefs)) $this->fielddefs=iddiMySql::getEntityDefinition($this->entityname);
              return $this->fielddefs[$inField];
          }
          /**
          * @desc Static method to get an entity
          */
          static function GetEntity($inEntityName){
              if (!self::$entities[$inEntityName]){
                  self::$entities[$inEntityName]=new iddiEntityDefinition($inEntityName);
              }
              return self::$entities[$inEntityName];
          }
          /**
          * @desc Static method to get field def given entityname and field
          * @param string $inEntityName The name of the entity we want
          * @param string $inFieldName The name of the field on that entity
          * @return iddiEntityField
          */
          static function Get($inEntityName,$inFieldName){
              $e=self::GetEntity($inEntityName);
              //if (!$e->fielddefs[$inFieldName]) throw new iddiCodingException('Field '.$inFieldName.' not found on '.$inEntityName,'iddi.code.iddiEntityDefinition.GetFieldDefinition.FieldDoesNotExist');
              return $e->fielddefs[$inFieldName];
          }
    }
