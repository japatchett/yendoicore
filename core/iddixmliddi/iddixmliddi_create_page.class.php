<?php

/**
 * Adds ability to force creation of webpages that are required by the given template
 * Useful especially for forms, such as contact forms, where you could force the
 * creation of /contact_us and /thank_you with some initial
 *
 * @example <iddi:create-page virtualfilename="/shop/basket" ident="shopbasket" pagetitle="Basket" entity="Web Page" template="../iddi-project/templates/templatename.xml"/>
 * @param string virtualfilename URL of the page to create
 * @param string ident Unique identifier for this page. ONCE SET DO NOT CHANGE. This is used to enable changed to url or pagetitle or other attributes
 * @param string pagetitle The title of the page!
 * @param string entity Optional The entity to create. If not provided the current ds entity will be used
 * @param string template optional The template to use for the page. If not provided the current template will be used
 *
 * @author J.Patchett
 */

class iddiXmlIddi_Create_Page extends iddiXmlIddiNode{
    function compile(){
        $url=$this->attributes['VIRTUALFILENAME'];
        $entity=new iddiEntity();
        echo 'Create Page '.$url;
        try{
            $entity->loadByVirtualFilename($url);
            //If we're still here the page already exists so we just need to check a few
            //things. Otherwise we'll have an exception and we can go ahead and create
            if($url!=$entity->virtualfilename) throw new iddiException('nf');
            echo 'Exists';
        }catch(Exception $ex){
            $entity=new iddiEntity();
            $entity->id=-1;
            $entity->virtualfilename=$url;
        }
            $entity->pagetitle=$this->getAttribute('PAGETITLE');
            $entity->entityname=$this->getAttribute('ENTITY');
            $entity->templatefile=$this->getAttribute('TEMPLATEFILE');
            //If the entity is blank set to the current entity
            if($entity->entityname=='') $entity->entityname=$this->owner->_e->entityname;
            //If the template is blank set to the current templatefile
            if($entity->templatefile=='') $entity->templatefile=$this->owner->getTemplate()->filename;
        //Run through child nodes looking for create-page-content nodes
        foreach($this->children as $child){
            if($child->nodename=='create-page-content'){
                $name=$child->getAttribute('NAME');
                $entity->$name=$child->value;
                $entity->dbfields[$name]=$child->value;
            }
        }
        echo $entity->dump();
        $entity->save();
        parent::compile();
    }
}