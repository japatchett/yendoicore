<?php
    /**
    * iddiXmlIddi_Select Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Select extends iddiXmlIddi_List_Items{
      function preparse(){
        $option=new iddiXmlNode($this->owner,'option',$this,array('value'=>'{id}'));
        $option->processAVT();
        $text=new iddiXmlIddi_Value_of($this->owner,'value-of',$option,array('select'=>$this->getAttribute('select')));
        $text->processAVT();
        parent::preparse();
      }
      function output(){
        $output='<select>';
        $output.=parent::output();
        $output.='</select>';
        return $output;
      }
    }
   