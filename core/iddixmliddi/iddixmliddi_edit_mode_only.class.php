<?php
    /**
    * iddiXmlIddi_Edit_Mode_Only Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Edit_Mode_Only extends iddiXmlIddiNode{
        function output(){
            if(iddiRequest::getMode()=='edit'){
                switch ($this->getAttribute('display')){
                    case 'floater':
                        return '<div class="iddi-editmode-item" style="position:fixed;bottom:20px;right:20px;padding:40px;background:rgba(255,255,255,0.5);border:solid 2px rgba(0,0,0,0.5)">'.parent::output().'</div>';
                        break;
                    default:
                        return parent::output();
                        break;
                }
            }
        }
    }
