<?php
    /**
    * iddiXmlIddi_Value Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Value extends iddiXmlIddiEditableNode{
        var $fieldname,$fieldvalue;
        function setup($owner,$tag,$parent,$attributes){
          parent::setup($owner,$tag,$parent,$attributes);
          if (!$this->owner)  throw new iddiException('This value node is not part of a valid document','iddi.iddiXml.IddiValue.setup.noowner'.$this);
          //if (!$this->owner->getCurrentEntity())  throw new iddiException('This value node is not part of a valid entity','iddi.iddiXml.IddiValue.setup.noentity',$this);
          if ($this->owner->getCurrentEntity()) $this->owner->getCurrentEntity()->addField($this);
        }
        function parse(){
          try{
            if (!$this->owner)  throw new iddiException('Node dosn\'t have an owner','iddi.iddiXml.IddiValue.parse.noowner',$this);
            if ($this->owner->getCurrentEntity())
            {
                $this->processAVT();
                $this->fieldname=iddiMySql::tidyname($this->attributes['NAME']);
                $this->fieldvalue=$this->getFieldValue($this->fieldname);

                /**
                 * Clean up the html to make as valid as possible
                 *
                 * Check BR tags
                 * Remove font tags
                 * Change b to strong
                 * Change i to em
                 * Check img tags for self close and alt
                 */
                if(iddiXmlDocument::$is_html){
                    $this->fieldvalue=str_replace('<br/>','<br>',$this->fieldvalue);
                }else{
                    $this->fieldvalue=str_replace('<br>','<br/>',$this->fieldvalue);
                }
                $this->fieldvalue=preg_replace('/<font(.*?)>/','',$this->fieldvalue);
                $this->fieldvalue=str_replace('</font>','',$this->fieldvalue);

                $editmode=iddiRequest::$current->getMode();
                if($this->attributes['STRIPTAGS']>0) $editmode='locked';
                if($this->attributes['CHOPLENGTH']>0) $editmode='locked';
                if($this->attributes['HIDDEN']) $editmode='hidden';

                try{
                  $d=$this->getdatasource();
                  iddiDebug::dumpvar('datasource', $d);
                  //$entid=($this->owner->getCurrentEntity())?$this->owner->getCurrentEntity()->getFieldValue('id'):'NEW';
                  $entid=$d->id;
                  $entname=$d->entityname;

                  //Get the full definition for the field
                  if($entname=='') $entname=iddiMySql::getEntityType($d->id);
                  $this->field_definition=iddiEntityDefinition::Get($entname,$this->fieldname);
                }catch(Exception $e){
                }

                if($editmode=='edit' || $editmode=='hidden') $f=new iddiPostVar($entname,$this->fieldname,$this->fieldvalue,$entid,$this->field_definition);
                switch($editmode) {
                  case 'edit':
                      $this->wrapIfNecessary();


                      $f->type=$this->field_definition->type;
                      $f->addattributes($this->attributes);
                      $fid=$f->postfieldid;

                      if ($this->fieldvalue==''){
                          if ($this->field_definition->help!=''){
                              $this->fieldvalue=$this->field_definition->help;
                          }else{
                              $this->fieldvalue='Enter a value for '.$this->fieldname;
                          }

                      }
                      switch($this->field_definition->type){
                          case 'lookup':
                              $lookup=$this->field_definition->lookup;
                              $sql="SELECT id,pagetitle from {PREFIX}sysfilenames WHERE entityname='{$lookup}'";
                              $d=iddiMySql::query($sql);
                              $value='<select id="iddinode_'.$this->_node_id.'"><option/>';
                              if($d->hasData()){
                                  foreach($d as $row){
                                      $value.='<option value="'.$row->id.'">'.$row->pagetitle.'</option>';
                                  }
                              }
                              $value.='</select>';
                              $this->value=$value;
                              iddiRequest::getform()->linkVar('iddinode_'.$this->_node_id,$f);
                              break;
                          default:
                              $this->value=$this->fieldvalue;
                              if ($this->parent){
                                  $parentnodeidattr=$this->parent->getAttribute('ID');
                                  if ($parentnodeidattr=='') $parentnodeidattr=$this->parent->attributes['id']='iddinode_'.$this->_node_id;
                                  $this->parent->setAttribute('contentEditable', 'true');
                                  iddiRequest::getform()->linkVar($parentnodeidattr,$f);
                              }
                      }

                      break;
                  default:
                      if ($this->field_definition->type=='lookup'){
                        $lookup=$this->field_definition->lookup;
                        try{
                          if($this->fieldvalue=='') $this->fieldvalue=0;
                          $e=new iddiEntity();
                          $e->loadById($lookup,$this->fieldvalue);
                          $this->setDataSource($e);
                          $this->fieldvalue=$e->pagetitle;
                        }catch(Exception $e){
                          $this->fieldvalue='n/a';
                        }
                        if ($this->children) $this->fieldvalue='';
                      }
                      if ($this->field_definition->type=='date'){
                        $this->fieldvalue=date('F jS Y',strtotime($this->fieldvalue));
                      }
                      if ($this->attributes['HIDDEN'] || $this->field_definition->hidden==1){
                          $this->value='';
                      }else{
                          $this->attributes['DEFAULT']=$this->getAttribute('ISBLANK');
                          if($this->fieldvalue=='' && $this->getAttribute('ISBLANK')){
                            $text=($this->attributes['DEFAULT']=='Lorem ipsum')?'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed auctor mauris. Donec pulvinar, dolor nec luctus iaculis, lacus odio aliquet massa, fermentum tincidunt purus erat id sapien. Aenean in nisi felis. Mauris euismod fermentum nisl eu vestibulum. Duis ullamcorper molestie faucibus. Nam varius ante eu quam euismod suscipit. Etiam iaculis sem id neque elementum id tincidunt tellus porttitor. Vivamus at justo arcu, sed varius nisl. Nam ut odio id turpis congue viverra. Suspendisse at tortor in mauris semper rutrum vel eget nulla. Quisque in lectus vestibulum neque dictum pretium et nec lorem. Morbi vel fringilla leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vitae eros ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam interdum ante ac lorem ultricies ut tempor mauris imperdiet. Praesent lobortis, magna sit amet cursus egestas, est ante rutrum odio, ut vestibulum mauris ligula ac arcu. Nam et sem nec arcu gravida facilisis a quis sapien. Quisque felis nibh, placerat gravida dignissim vel, scelerisque in eros.</p>
  <img src="/iddi/no-pic.jpg" style="float:right;" alt="lorem" width="200"/>
  <p>Nulla pulvinar ullamcorper elit, in interdum justo sagittis nec. Nam quis diam at nunc semper ultrices. Integer tincidunt vulputate velit a cursus. Ut vestibulum dictum metus, quis scelerisque velit tincidunt in. Mauris ut nunc vestibulum elit varius rhoncus. Proin feugiat congue ullamcorper. Suspendisse nec ipsum eros. Maecenas id metus diam, gravida porta erat. Fusce semper egestas urna, vitae malesuada velit porta sed. Phasellus diam libero, tempor id suscipit sed, ultricies aliquet ipsum. Vestibulum malesuada ornare tortor, in convallis massa suscipit sed. Praesent euismod faucibus nulla pulvinar scelerisque. Fusce lacinia diam id felis fringilla tristique. Integer vestibulum dictum metus vitae vehicula. In a dolor lobortis ante consequat mollis eu eu lectus. Morbi id risus id lacus dictum venenatis. Integer tincidunt mi et magna posuere non porta metus aliquet.</p>
  <p>Donec turpis arcu, tempor at venenatis ut, convallis ac metus. Nulla pretium lectus sit amet justo pretium ac adipiscing ligula dictum. Nunc porta ligula id velit tempus laoreet vitae at nisl. Proin sed magna neque, eu hendrerit nisi. Duis tincidunt metus bibendum mi pharetra venenatis. Phasellus malesuada, nisi eget laoreet viverra, eros leo ultrices purus, sit amet aliquam tellus sapien id ante. Sed consequat porttitor hendrerit. Nunc porttitor, tellus eget feugiat tempus, ligula dolor pharetra nulla, sodales pellentesque eros magna non lacus. Praesent massa massa, fringilla eu feugiat at, viverra ut est. Ut lacinia ullamcorper turpis, quis blandit lectus laoreet at.</p>
  <p>Ut mollis aliquet mauris, eu molestie elit semper eget. Morbi eget velit nisl, eu facilisis enim. Curabitur laoreet, turpis at pharetra tincidunt, augue sem dapibus ligula, sit amet consectetur est enim quis nisl. Proin nec semper dui. Ut in aliquam arcu. Integer molestie nisi id turpis pharetra ac luctus eros pretium. Curabitur sed lacus risus, sit amet fermentum arcu. Sed cursus mauris faucibus justo elementum cursus. Cras vehicula sodales eleifend. Proin sit amet tellus lectus, ac ullamcorper neque. Sed varius placerat massa non rhoncus. Maecenas at diam vitae diam pellentesque sodales.</p>
  <p>Praesent egestas rutrum est, ut convallis urna interdum vitae. Praesent eu ante ante. Mauris lobortis sem ut est condimentum adipiscing. Sed rutrum ullamcorper lorem ac rhoncus. Sed eu felis mi, in cursus felis. Integer et hendrerit arcu. Donec at magna vitae urna fringilla tempus. Praesent at magna lorem, eu tincidunt nulla. Sed bibendum posuere dapibus. Proin nulla velit, elementum dignissim porta accumsan, condimentum vitae ante. Ut porttitor sodales tellus at faucibus. Integer at odio ut nisi dignissim sollicitudin. Nunc facilisis felis quis ante hendrerit scelerisque. Cras ac urna quis ante imperdiet placerat. Etiam auctor cursus augue, at lacinia erat aliquet vel. Ut arcu sem, tristique et adipiscing ut, commodo et libero. Mauris hendrerit augue placerat elit consequat rhoncus. Praesent viverra convallis auctor. Integer eu metus eu lorem vestibulum euismod.</p>':$this->attributes['DEFAULT'];
                            $this->fieldvalue=$text;
                          }
                          if($this->attributes['STRIPTAGS']=='yes') $this->fieldvalue=strip_tags($this->fieldvalue);
                          if($this->attributes['CHOPLENGTH']>0){
                              $v=$this->fieldvalue;
                              $vout='';
                              $vp=explode('</p>',$v);
                              $arrelem=0;
                              while(strlen($vout)<$this->attributes['CHOPLENGTH'] && $arrelem<sizeof($vp)){
                                  $v=strip_tags($vp[$arrelem]).' ';
                                  $l=strlen($v);
                                  $vout.='<p>'.(((strlen($vout)+$l)<$this->attributes['CHOPLENGTH'])?$v:substr($v,0,strpos($v,' ',$this->attributes['CHOPLENGTH'])).'...').'</p>';
                                  $arrelem++;
                              }
                              $this->value=$vout;
                          }else{
                              if($this->fieldvalue=='0'){
                                $this->value='0 ';
                              }else{
                                $this->value=$this->fieldvalue;
                              }
                          }
                      }
                      break;
                }
                if ($this->children) foreach($this->children as $child) $child->parse();
                $this->owner->getCurrentEntity()->addField($this);
            }
          }
          catch (Exception $e){}

      }
        function getName(){return iddiMySql::tidyname($this->attributes['NAME']);}
        function getCaption(){return ($this->attributes['CAPTION'])?$this->attributes['CAPTION']:$this->attributes['NAME'];}
        function getType(){return $this->attributes['TYPE'];}
        function compile(){
          iddi::Log('Compile : Compiling Value Node '.$this->getName().' at line '.$this->lineno,0);
          $this->owner->getCurrentEntity()->addField($this);
          if ($this->children) foreach($this->children as $child) $child->compile();
        }
    }
