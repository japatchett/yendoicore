<?php
    /**
    * iddiXmlIddi_Rss_Feed Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Rss_Feed extends iddiDataSource{
        var $a_source,$a_frequency,$a_name;
        var $localfilename;

        function preparse(){
            $this->a_source=$this->getAttribute('source');
            $this->a_frequency=$this->getAttribute('frequency');
            $this->a_name=$this->getAttribute('name');
            $this->localfilename=IDDI_PROJECT.'rss_cache/'.base64_encode($this->a_name);
            //Make sure we have the correct folders for caching the feed
            @mkdir(IDDI_PROJECT.'rss_cache');
            @chmod(IDDI_PROJECT.'rss_cache',0777);

            //Make sure it's all going to work
            if(!file_exists(IDDI_PROJECT.'rss_cache')) throw new iddiException('There is no RSS Cache folder','iddi.xml.iddiXmlIddiRssFeed.NoRSSCache');
            if(!is_writeable(IDDI_PROJECT.'rss_cache')) throw new iddiException('RSS Cache is not writable','iddi.xml.iddiXmlIddiRssFeed.RSSCacheNotWritable');

        }
        function parse(){
            $this->processAVT();
            //Look for the local source file in the rss cache
            $download=0;        //0=don't download  1=download now  2=background download - will be ready for next user hopefully

            if(file_exists($this->localfilename)){
                if (!file_exists($this->localfilename.'.async')){
                    if(filemtime($this->localfilename)<(time()-$this->a_frequency)){
                        $download=2;
                    }
                }
            }else{
                $download=1;
            }
            if ($download==1) $data=iddiRss::getFeed($this->a_source,$this->localfilename);
            if ($download==2) iddiRss::getFeedAsync($this->a_source,$this->localfilename);

        }
    }
   