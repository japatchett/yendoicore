<?php
    /**
    * iddiXmlIddi_Debug Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Debug extends iddiXmlIddiNode{
      function parse(){
          $this->processAVT();
          $this->selection=$this->getAttribute('SELECT');
          $d=$this->xpath($this->selection);
          foreach($d as $df){
              $clo=$df->clonenode($this,true);
              $this->appendChild($clo);
          }
      }
      function output(){
            if ($this->children) foreach($this->children as $child) $childoutput.=$child->output($clean,($clean)?$level+1:0);
            $childoutput=str_replace('<','&lt;',$childoutput);
            $childoutput=str_replace('>','&gt;',$childoutput);
            $childoutput=str_replace(' ','&nbsp;',$childoutput);
            $childoutput=str_replace("\n",'<br/>',$childoutput);
            return $childoutput;
      }
    }

    //---- FORMS ----//

   