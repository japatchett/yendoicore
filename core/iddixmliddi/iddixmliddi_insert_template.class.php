<?php
    /**
    * iddiXmlIddi_Insert_Template Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Insert_Template extends iddiXmlIddiNode{
        static $loadedtemplates;
        var $pp;
        protected $_a_sourcefile,$_a_xpath;

        function preparse(){
            if (!$this->pp){
                $this->processAVT();
                $this->pp=true;
                $this->_a_sourcefile=$this->getAttribute('source');
                $this->_a_xpath=$this->getAttribute('select');
                $this->_a_sourcefile.='.xml';
                $fullname=IDDI_FILE_PATH.'../iddi-project/templates/'.$this->_a_sourcefile;
                $fullnamelang=IDDI_FILE_PATH.'../iddi-project/templates/'.iddiRequest::$current->language.'/'.$this->_a_sourcefile;
                //if(file_exists($fullnamelang)) $fullname=realpath($fullnamelang);
                //die("inserting $fullname");
                iddiDebug::message('Insert Template '.$this->_a_sourcefile);
                if (!self::$loadedtemplates[$this->_a_sourcefile]){
                    self::$loadedtemplates[$this->_a_sourcefile]=new iddiTemplate($fullname,$this->getDataSource());
                }
                $t=self::$loadedtemplates[$this->_a_sourcefile];
                $n=$t->xpath($this->_a_xpath);
                foreach($n as $x){
                    $this->appendChild($x->cloneNode($this,true));
                }
                parent::preparse();
            }
        }

        function compile(){
            iddi::Log('Compile : Compiling Template Insert '.$this->sourcefile.' at line '.$this->lineno,0);
            $this->preparse();
            if ($this->children) foreach($this->children as $child) $child->compile();
        }
    }
    /**
    * @desc replaces the selected node in the document with the nodes inside this one.
    */
   