<?php
    /**
    * iddiXmlIddi_Filter Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Filter extends iddixmliddinode {
      function preparse(){
          $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
          if(substr_count($this->getAttribute('FILTER'),'language')>0) $qp->allow_non_root_items=true;
          parent::preparse();
      }
      function buildSql_Where(&$whereand=''){
          $this->processAVT();
          $whereandthis=($this->getAttribute('LOGIC'))?$this->getAttribute('LOGIC'):$whereand;
          $w=$whereandthis.$this->getAttribute('FILTER');
          $whereand=' AND ';
          return $w;
      }
    }
   