<?php
    /**
    * iddiXmlIddi_Img_Size Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Img_Size extends iddiXmlIddi_Img_Instruction{
        function preparse(){
            $p=$this->getParentOfType('iddiXmlIddi_Img');
            $p->fieldinfo[$this->_node_id]['resize']['width']=$this->getAttribute('WIDTH');
            $p->fieldinfo[$this->_node_id]['resize']['height']=$this->getAttribute('HEIGHT');
            $p->fieldinfo[$this->_node_id]['resize']['scalemode']=$this->getAttribute('SCALEMODE');
            $p->fieldinfo[$this->_node_id]['resize']['upscale']=($this->getAttribute('UPSCALE')=='false')?true:false;
        }
    }
