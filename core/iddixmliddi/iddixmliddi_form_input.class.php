<?php

class iddiXmlIddi_Form_Input extends iddiXmlIddi_Value{
    function output(){
            $this->outputid=$this->attributes['ID'];
            $this->outputclass=$this->attributes['CLASS'];
            $this->outputhidden=$this->attributes['HIDDEN'];
            $this->outputtitle=$this->attributes['TITLE'];
            $this->outputtype=$this->attributes['TYPE'];
            
            $this->fieldid=iddiMySql::tidyname($this->attributes['NAME']);

            //If we have a post grab value from there instead
            if($_POST['iddif_'.$this->fieldid]){
                $this->fieldvalue=$_POST['iddif_'.$this->fieldid];
                echo($this->fieldvalue);
            }

            //switch($this->field_definition->type){
            switch($this->outputtype){
               case 'html':
                    $this->outputtype='textarea';
                    $output='<textarea name="'.$this->fieldid.'">'.$this->fieldvalue.'</textarea>';
                    break;
               case 'lookup':
                    $this->lookupitems=iddiMySql::query("SELECT id,pagetitle FROM {PREFIX}sysfilenames WHERE entityname='".$this->attributes['ENTITY']."'");
                    $output='<select name="iddif_'.$this->fieldid.'">';
                    $output.='<option value="">Please select</option>';
                    foreach($this->lookupitems as $item){
                        $output.='<option value="'.$item->id.'"';
                        if($item->id==$this->fieldvalue) $output.=' selected="selected"';
                        $output.='>'.$item->pagetitle.'</option>';
                    }
                    $output.='</select>';
                    break;
               case 'image':
               case 'file':
                    $this->outputtype='file';
                    $output='<input type="file" name="iddif_'.$this->fieldid.'" id="'.$this->attributes['ID'].'"';
                    if ($this->outputclass!='') $output.=' class="'.$this->outputclass.'"';
                    if ($this->outputtitle!='') $output.=' title="'.$this->outputtitle.'"';
                    $output.='/>';
                    break;
               default:
                    $output='<input type="text" name="iddif_'.$this->fieldid.'" id="'.$this->attributes['ID'].'"';
                    if ($this->outputclass!='') $output.=' class="'.$this->outputclass.'"';
                    if ($this->outputtitle!='') $output.=' title="'.$this->outputtitle.'"';
                    $output.=' value="'.strip_tags($this->fieldvalue).'"';
                    $output.='/>';
                    break;
            }


            return $output;
    }
}