<?php
    /**
    * iddiXmlIddi_If_Value Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_If_Value extends iddiXmlIddi_If_Base{
      function test_if(){
        $this->processAVT();
        $xpathquery=$this->getAttribute('SELECT');
        $d=$this->getdatasource();
        if ($d==null){
          return false;
        }else{
          $v1=$d->xpath($xpathquery);
          if ($v1){
              $v=$v1->first()->value;
              return ($v!='');
          }
        }
        return false;
      }
    }
