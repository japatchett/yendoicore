<?php
    /**
    * iddiXmlIddi_Join_Entity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Join_Entity extends iddixmliddinode {
      function buildSql_Joins(){
          return ', `{PREFIX}'.$this->getAttribute('ENTITY').'` as `'.$this->getAttribute('ENTITY').'`';
      }
      function buildSql_Joins_Where(&$whereand=''){
        $m=$this->getAttribute('MATCH');
          $w=$whereand.'('.$m.')';
          $whereand=' AND ';
          return $w;
      }
    }
   