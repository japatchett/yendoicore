<?php
    /**
    * iddiXmlIddi_Input Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Input extends iddiXmlIddiNode{
        var $outputid,$outputclass,$outputtype,$outputhidden,$outputtitle;

        function parse(){

           //On the parse we just want to setup the outputs
            if (!$this->owner)  throw new iddiException('Node dosn\'t have an owner','iddi.iddiXml.IddiValue.parse.noowner',$this);
            if ($this->owner->getCurrentEntity())
            {
                //Process the Attribute Values
                $this->processAVT();
                //Find out what data we are looking for
                $this->fieldname=iddiMySql::tidyname($this->attributes['NAME']);
                $this->fieldvalue=$this->getFieldValue($this->fieldname);
                //Get the datasource so that we know what entity we are dealing with
                $d=$this->getdatasource();
                $entid=$d->id;
                $entname=$d->entityname;
                //Get the full definition for the field
                $this->field_definition=iddiEntityDefinition::Get($entname,$this->fieldname);

                //Now add a new postvar into the session - this will generate an ID that we can use
                //$f=new iddiPostVar($entname,$this->fieldname,$this->fieldvalue,$entid,$this->field_definition);
                //$f->type=$this->attributes['TYPE'];
                //$f->addattributes($this->attributes);
                //$this->fieldid=$f->postfieldid;
                $this->fieldid=$this->fieldname;
                $fieldname=$this->fieldname;
                $this->value=$d->$fieldname;

                //echo $d->dump(2);

                //If we have a lookup - go grab the items
                if($this->field_definition->type=='lookup'){
                    if(!$this->field_definition->lookup) throw new iddiTemplateException('You must specify an entity to use in a lookup','iddi.xml.iddiinput.noentity');
                    if(substr($this->field_definition->lookup,0,1)=='['){
                        $items=explode(',',substr($this->field_definition->lookup,1,-1));
                        foreach($items as $item){
                            $this_item=explode('|',$item);
                            $new_item=new stdClass();
                            $new_item->id=$this_item[0];
                            $new_item->pagetitle=$this_item[1];
                            $all_items[]=$new_item;
                        }
                        $this->lookupitems=$all_items;
                    }else{
                        $sql='SELECT max(d.id) as id,d.pagetitle FROM {PREFIX}sysfilenames d WHERE deleted=0 AND entityname=\''.$this->field_definition->lookup.'\' GROUP BY d.pagetitle';
                        $this->lookupitems=iddiMySql::query($sql);
                    }
                }

                //Create a field link for any javascript to sync fields on the form and to the server
                $parentnodeidattr=$this->parent->getAttribute('ID');
                if ($parentnodeidattr=='') $parentnodeidattr=$this->parent->attributes['ID']='iddinode_'.$this->_node_id;
                iddiRequest::getform()->linkVar($parentnodeidattr,$f);

                //Parse the children
                if ($this->children) foreach($this->children as $child) $child->parse();

                //Add this field to the entity
                $e=$this->getCurrentEntity();
                if($e) $e->addField($this); else throw new iddiException('Input has no target entity','iddi.xml.form.input.noentity');
            }
        }
        function getName(){return iddiMySql::tidyname($this->attributes['NAME']);}
        function getCaption(){return $this->attributes['NAME'];}
        function getType(){return $this->attributes['TYPE'];}

        function output(){
            //We need to generate the correct xhtml output with a name of the fieldid


            $this->outputid=$this->attributes['ID'];
            $this->outputclass=$this->attributes['CLASS'];
            $this->outputhidden=$this->attributes['HIDDEN'];
            $this->outputtitle=$this->attributes['TITLE'];

            switch($this->field_definition->type){
               case 'html': $this->outputtype='textarea';
                    $output.='<textarea name="'.$this->fieldid.'">'.$this->fieldvalue.'</textarea>';
                    break;
               case 'lookup':
                    $output.='<div class="input text '.$class.'"><label><span>'.($this->field_definition->caption).'</span>';
                    $output.='<select name="'.$this->fieldid.'">';
                    $output.='<option value="">Please select</option>';
                    foreach($this->lookupitems as $item){
                        $output.='<option value="'.$item->id.'"';
                        if($item->id==$this->fieldvalue) $output.=' selected="selected"';
                        $output.='>'.$item->pagetitle.'</option>';
                    }
                    $output.='</select></label></div>';
                    break;
               case 'check':
               case 'bool':
                    $output.='<div class="input bool '.$class.'"><span>'.($this->field_definition->caption).'</span>';
                    $output.='<label><input type="radio" value="0" name="'.$this->fieldid.'"'.(($this->fieldvalue!=1)?' checked="checked"':'').'>No</label>';
                    $output.='<label><input type="radio" value="1" name="'.$this->fieldid.'"'.(($this->fieldvalue==1)?' checked="checked"':'').'>Yes</label>';
                    $output.='</div>';
                    break;
               case 'image':
               case 'file':
                    $this->outputtype='file';
                    $output.='<input type="file" name="iddif_'.$this->fieldid.'" id="'.$this->attributes['ID'].'"';
                    if ($this->outputclass!='') $output.=' class="'.$this->outputclass.'"';
                    if ($this->outputtitle!='') $output.=' title="'.$this->outputtitle.'"';
                    $output.='/>';
                    break;
               default:
                    $class='medium';
                    if($this->field_definition->maxlength>200) $class='long';
                    if($this->field_definition->maxlength<20) $class='small';
                    $output.='<div class="input text '.$class.'"><label><span>'.($this->field_definition->caption).'</span><input type="text" name="'.$this->fieldid.'" id="'.$this->attributes['ID'].'"';
                    if ($this->outputclass!='') $output.=' class="'.$this->outputclass.'"';
                    if ($this->outputtitle!='') $output.=' title="'.$this->outputtitle.'"';
                    $output.=' value="'.strip_tags($this->fieldvalue).'"';
                    if($this->field_definition->maxlength > 0) $output.=' maxlength="'.$this->field_definition->maxlength.'"';
                    $output.='/></label></div>';
                    break;
            }


            return $output;
        }

        function compile(){
            echo '<li>Field:'.$this->getName();
          iddi::Log('Compile : Compiling Value Node '.$this->getName().' at line '.$this->lineno,0);
          $this->owner->getCurrentEntity()->addField($this);
          if ($this->children) foreach($this->children as $child) $child->compile();
        }
    }

   