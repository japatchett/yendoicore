<?php
/**
 * iddiXmlIddi_Access_Control_Deny Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 **/
class iddiXmlIddi_Access_Control_Deny extends iddiXmlIddiNode {
    var $editable=false;
    protected $_force_allow=false;

    function preparse() {
        $all=$this->xpath('.//value',true);
        foreach($all as $one) if ($one->nodename=='value') $this->editable=true;
    }
    function output() {
        if(iddiRequest::getMode()=='edit' || $this->_force_allow) {
            $output='<div class="editor-note"><p class="text">This section displays if the current visitor is not allowed access to any of the above</p>';
        }
        $output.=parent::output();
        if(iddiRequest::getMode()=='edit') {
            $output.='</div>';
        }
        return $output;
    }

    function get_editor_json($json) {
        $this->_force_allow=true;

        $json_obj=new stdClass();
        $json_obj->name='access_control_deny';
        $json_obj->content=$this->output();
        //Build the privileges list that trigger this block
        $json_obj->privileges['DENY']='Access Denied';

        //Add to the access control block if available
        //Otherwise just to the json object
        $access_control_block=$this->getParentOfType('iddiXmlIddi_access_control');
        if($access_control_block) {
            $access_control_block->_json_add_ac_state($json_obj);
        }else {
            $json->editors[]=$json_obj;
        }

        $this->_force_allow=false;
    }

}
