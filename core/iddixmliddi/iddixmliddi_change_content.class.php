<?php
    /**
    * iddiXmlIddi_Change_Content Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Change_Content extends iddiXmlIddiNode{
        protected $pp;

        function preparse(){
            if (!$this->pp){
                $this->pp=true;
                $xpath=$this->getAttribute('select');
                $targets=$this->xpath($xpath,true);
                foreach($targets as $tgtnode) $tgtnode->changeContent($this->children);
                $this->parent->removeChild($this);
            }
        }
    }
   