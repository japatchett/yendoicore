<?php
    /**
    * iddiXmlIddi_List_Repeat Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_List_Repeat extends iddiXmlIddiNode{
        var $is_origin=false;
        function preparse(){
            $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
            $qp->repeatable=$this;
        }

        function output($output){
          if(!$this->is_origin) return parent::output($output);
        }
    }
   