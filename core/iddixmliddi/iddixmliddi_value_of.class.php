<?php
    /**
    * iddiXmlIddi_Value_Of Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Value_Of extends iddiXmlIddiNode{
      function parse(){
          $this->processAVT();
          $xpathquery=$this->getAttribute('SELECT');
          $strip_tags=$this->getAttribute('TAGS');
          $maxwords=$this->getAttribute('MAXWORDS');
          $maxchars=$this->getAttribute('MAXCHARS');
          $d=$this->getdatasource();
          if ($d==null){
              $a=$this->xpath($xpathquery,true);
              $this->appendChild($a);
          }else{
            //if (!is_a($this->getDataSource(),'iddiDataSource'))  throw new iddiException(get_class($this->getDataSource()).' is an invalid data source','iddi.iddiXml.valueof.invaliddatasource',$this);
            $v1=$d->xpath($xpathquery);
            if ($v1){
                $v=$v1->first()->value;
                if ($strip_tags) $v=strip_tags($v,$strip_tags);
                if ($maxchars>0){
                    $v=substr($v,0,strrpos($v,' ',$maxchars));
                }
                $this->value=$v;
            }
          }
      }
    }
   