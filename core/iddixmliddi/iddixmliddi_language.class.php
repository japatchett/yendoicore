<?php
    /**
    * iddiXmlIddi_Language Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Language extends iddiXmlIddiNode{
      static $languagedata;
      static $baselanguagedata;
      function preparse(){
        if(!isset(self::$languagedata)){
           //Load the language file
           $lang=iddiRequest::$current->language;
           $filename=IDDI_PROJECT_PATH.'/languages/'.$lang.'.xml';
           if(file_exists($filename)){
            self::$languagedata=new iddiXmlDocument();
            self::$languagedata->LoadFile($filename);
           }else{
            if(iddiRequest::$current->is_compiling) throw new iddiTemplateException('Language file '.$filename.' does not exist','language_file.does_not_exist');
           }
        }
        if(!isset(self::$baselanguagedata)){
           //Load the language file
           $lang=iddiRequest::$current->baselanguage;
           $filename=IDDI_PROJECT_PATH.'/languages/'.$lang.'.xml';
           if(file_exists($filename)){
             self::$baselanguagedata=new iddiXmlDocument();
             self::$baselanguagedata->LoadFile($filename);
           }else{
            if(iddiRequest::$current->is_compiling) throw new iddiTemplateException('Language file '.$filename.' does not exist','language_file.does_not_exist');            }
        }
        if($this->value==''){
          if (self::$languagedata) $this->value=self::$languagedata->xpath('.//v[@n=\''.$this->getAttribute('select').'\']')->first()->value;
          if($this->value==''){
            if (self::$baselanguagedata) $this->value=self::$baselanguagedata->xpath('.//v[@n=\''.$this->getAttribute('select').'\']')->first()->value;
          }
          if($this->value=='') $this->value=$this->getAttribute('select');
        }
      }
    }
   