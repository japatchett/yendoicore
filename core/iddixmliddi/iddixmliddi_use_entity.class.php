<?php
    /**
    * iddiXmlIddi_Use_Entity Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Use_Entity extends iddiXmlIddiNode{
       function parse(){

           $this->processAVT();
           $this->entityname=$this->getAttribute('name');
           $this->entityid=$this->getAttribute('id');
           $this->entity=new iddiEntity();

           if($this->entityid=='') $this->entityid=$this->getAttribute('default');

           if($this->entityid!=''){
                try{
                    $this->entity->loadById($this->entityname,$this->entityid);
                }catch(Exception $ex){
                    try{
                        $this->entity=new iddiModel();
                        $this->entity->entityname=$this->entityname;
                        $this->entity->get_by_id($this->entityid);
                    }catch(Exception $ex){
                        $this->pagetitle='Not Found';
                    }
                }
           }
           $this->setDataSource($this->entity);
           parent::parse();
       }
    }
