<?php
    /**
    * iddiXmlIddi_Config_Value Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Config_Value extends iddiXmliddiNode{
      function preparse(){
        $section=$this->getAttribute('section');
        $value=$this->getAttribute('value');
        $this->value=iddiconfig::GetValue($section,$value);
      }
    }
   