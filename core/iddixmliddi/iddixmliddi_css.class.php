<?php
    /**
    * iddiXmlIddi_Css Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Css extends iddiXmlIddiNode{
      function compile2(iddiTemplate $template){
        echo "<li>Compiling CSS</li>";
        $entity=$this->getParentOfType('iddiXmlIddi_Entity');
        //Build the data
        $data1="/* CSS for ".str_replace('.xml','',basename($template->filename))." */\n";
        $data2=$this->value;
        $data3="/* END for ".str_replace('.xml','',basename($template->filename))." */\n";
        //Load the css file, remove the section for this template, and append the new data
        $filename=IDDI_PROJECT_PATH.'/assets/css/'.$this->getAttribute('name').'.css';
        $file=file($filename);
        $fp=fopen($filename,'w');
        $writing=true;
        $firstwrite=true;
        foreach($file as $line){
          if(trim($line)==trim($data1)) $writing=false;
          if(trim($line)==trim($data1) && $firstwrite){
            fwrite($fp,trim($data1).$data2."\n".$data3);
            $firstwrite=false;
          }
          if($writing) fwrite($fp,$line);
          if(trim($line)==trim($data3)) $writing=true;
        }
        if($firstwrite) fwrite($fp,"\n".trim($data1).$data2."\n".$data3);
        //Save it back
        fclose($fp);
        parent::compile2($template);
      }
    }

    //---- Image Handling stuff ----//
   