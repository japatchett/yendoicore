<?php
    /**
    * iddiRequest_iddi_help Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRequest_iddi_help extends iddiRequest_resource{
      function getResponse(){
        return $this->_getResponse($_GET['entity'],'help.xslt');
      }
    }

   