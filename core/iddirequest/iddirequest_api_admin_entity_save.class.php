<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_save extends Api_Result{
    function output(){
        $eclass='iddiEntity_'.$_POST['entityname'];
        if(class_exists($eclass)){
            $entity=new $eclass();
            $entity->get_by_id($_POST['entityid']);
        }else{
            $entity=new iddiEntity($_POST['entityname'],$_POST['entityid']);
        }

        unset($_POST['entityname']);
        unset($_POST['entityid']);
        foreach($_POST as $k=>$v){
            $entity->$k=$v;
        }

        iddiMySql::saveEntity($entity);

        die('Done');

    }
}