<?php
    /**
    * iddiRequest Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRequest extends iddiEvents{
        const BEFORE_GET_RESPONSE='BeforeGetResponse',
              GET_RESPONSE_BEFORE_USE_CACHED_RESPONSE='GetResponse_BeforeUseCachedResponse',
              AFTER_GET_RESPONSE_BEFORE_REPLY='AferGetResponse_BeforeReply';

      static $form;
      /**
      * @desc The current active request
      * @var iddiRequest
      */
      static $current;
      /**
      * @desc The current active response
      * @var iddiResponse
      */
      static $currentresponse;
      var $page,$pages,$pageuri,$p;
      var $language='en-gb';
      var $baselanguage='en-gb';
      var $editlanguage='cym';
      var $starttime,$memory;
      var $requestid=0;
      var $is_compiling=false;
      var $mainrequest,$ismainrequest=false,$isajax=false,$usecache=false;

      function __construct($ismain=false){
            $this->requestid=++$_SESSION['reqid'];
            $this->starttime=microtime();
            $this->memory=memory_get_usage(true);



            //Make this the current request
            self::$current=$this;

            if (!$ismain) $this->mainrequest=self::getMainRequest();
            if ($ismain) $this->makeMainRequest();

            //Reparse the request vars that will have been destroyed by rewrite
            $pagebits=explode('?',$_SERVER['REQUEST_URI']);
            $this->pageuri=$pagebits[0];
            $pagevars=explode('&',$pagebits[1]);
            foreach($pagevars as $pagevar){
                $varbits=explode('=',$pagevar);
                $varname=$varbits[0];
                if ($varname!=''){
                    $_REQUEST[$varname]=$varbits[1];
                    $_GET[$varname]=$varbits[1];
                    $this->$varname=$varbits[1];
                }
            }
            foreach($_POST as $k=>$v){
                $this->$k=$v;
            }
            $this->REQUEST=$_REQUEST;
            $this->SESSION=$_SESSION;
            //Language Switching
            if(!$_REQUEST['ajax']){
                if ($_COOKIE['iddi_language']) $this->language=$_COOKIE['iddi_language'];
                if ($_REQUEST['set_lang']){
                    setcookie('iddi_language',$_REQUEST['set_lang'],time()+(60*60*24*100));
                    $this->language=$_REQUEST['set_lang'];
                }
            }
            //Switch language from slices
           $slices=explode('/',$this->pageuri);
           if(file_exists(IDDI_PROJECT_PATH.'/languages/'.$slices[1].'.xml')){
             //If yes switch language
             iddiRequest::$current->language=$slices[1];
           }
           if(iddiRequest::$current->language=='') iddiRequest::$current->language='en-gb';

            //Determine response type
            if ($_POST['ajax']==1 || $_REQUEST['ajax']==1) $this->isajax=true;

            //TODO:Layout Switching
            //Layout switching will cause template searching to check templatefolder/skin/language, then templatefolder/skin then tempaltefolder/language then templatefolder

            //For the debugger
            if (iddi::$debug) iddiDebug::dumpvar('Request for '.$this->pageuri,$this);

      }
      function __destruct(){
          //if($this->pages) foreach($this->pages as $p) $p->destroy();
          //if($this->page) $this->page->destroy();
          //unset($this->page);
          //unset($this->pages);
      }

        function __sleep() {
            unset($this->SESSION);
            return( array_keys( get_object_vars( $this ) ) );
        }


      static function startNewMainRequest(){
          $r=new iddiRequest($this);
          self::storeMainRequest($r);
          return $r;
      }
      static function storeMainRequest($req){self::$form=null;unset($_SESSION['iddi-form']);unset($req->mainrequest);$req->ismainrequest=true;$_SESSION['iddi-request']=$req;}
      static function getMainRequest(){$req->ismainrequest=false;return $_SESSION['iddi-request'];}
      static function getform(){
          if (!self::$form){
              if ($_SESSION['iddi-form']) self::$form=$_SESSION['iddi-form']; else self::$form=new iddiform();
          }
          return self::$form;
      }
      static function addDataSource($d){
          if (!isset(self::getMainRequest()->page)) self::getMainRequest()->page=$d;
          self::getMainRequest()->pages[$d->id]=$d;
          return $d;
      }
      static function getCurrentDataSource(){
          return self::getMainRequest()->page;
      }
      static function getDataSource($id){
          foreach(self::getMainRequest()->pages as $page){
            if ($page->id==$id) return $page;
          }
          if($id>0){
              $ent=new iddiEntity();
              $ent->loadById('',$id);
              self::getMainRequest()->pages[$id]=$ent;
              return $ent;
          }else{
              $ent=new iddiEntity();
              $ent->id=$id;
              self::getMainRequest()->pages[$id]=$ent;
              return $ent;
          }
          return null;
      }
      function setDevMode(){
          $this->serveraddress=$_SERVER['SERVER_ADDR'];
          $this->remoteaddress=$_SERVER['REMOTE_ADDR'];
          if($this->serveraddress==$this->remoteaddress) $this->devmode=true;
          $sparts=explode('.',$this->serveraddress);
          array_pop($sparts);
          $rparts=explode('.',$this->remoteaddress);
          array_pop($rparts);
          if(implode('.',$sparts)===implode('.',$rparts)) $this->devmode=true;

          return $this->devmode;
      }

      /**
      * @desc Make this request the main request for future ajax calls - USE WITH EXTREME CAUTION
      * @return iddiRequest Returns itself for chaining
      */
      function makeMainRequest(){
          self::storeMainRequest($this);
          return $this;
      }
      /**
      * @desc Gets the mode of the current request, will be either 'edit' or 'view'
      */
      function getMode(){
          //$m=($_REQUEST['mode'])?$_REQUEST['mode']:'view';
          $u=$_SESSION['user'];
          $m=($u && substr_count($u->getValue('accesslevels'),'admin') && !$_REQUEST['ajax'])?'edit':'view';
          $_SESSION['mode']=$m;
          return $m;
      }
      /**
      * @desc Gets the context of the current request
      */
      function getContext()
      {
          return ($_POST['context'])?$_POST['context']:$_REQUEST['context'];
      }
      /**
      * @desc Gets the response for the current request.
      */
      function getResponse(){
          $response=new iddiResponse($this);
          self::$currentresponse=$response;

//First thing is to check for a user logging in or out
          iddiUser::startup();

          $this->setDevMode();

          $e=$this->trigger(self::BEFORE_GET_RESPONSE);
          if (!$e->cancelled)
          {
              if (!$this->usecache){
                  try{
                      //See if we have a post
                      if($_POST){
                          if (iddi::$debug) iddiDebug::dumpvar('Data posted',$_POST);
                          //Grab the currently active form
                          $form=iddiRequest::getform();
                          //fieldupdate will contain a json encoded object
                          if($_POST['fieldupdate']){
                              //Decode the json
                              $json_post=json_decode($_POST['fieldupdate']);
                              if (iddi::$debug) iddiDebug::dumpvar('JSON posted',$json_post);
                              if($json_post){
                                  if ($json_post->values){
                                      //Run through each json value and assign it's value as if it were a posted value - it will then be dealt with in a momenet
                                      foreach($json_post->values as $json_value){
                                        if ($json_value){
                                            //Add to the main post
                                            $_POST[$json_value->postfieldid]=$json_value->value;
                                        }
                                      }
                                  }
                                  iddiDebug::dumpvar('JSON post added to main post. Details contain new $_POST',$_POST);
                              }else{
                                  iddiDebug::message('Invalid JSON.');
                              }
                              if (iddi::$debug) iddiDebug::dumpvar('Updating data store',$form);
                          }
                          if (iddi::$debug) iddiDebug::dumpvar('Merging post with stored form. Details contain form',$form);
                          //Now the main form processor
                          foreach($_POST as $post_key=>$post_value){
                            $post_key=str_replace('iddif_','',$post_key);
                            //$changed=$form->values[$post_key]->changed;
                            //Make sure it's a value we're looking for and not one a hacker has introduced
                            if($form->values[$post_key]){
                                //See if the value has changed from the one stored
                                $post_value=stripslashes(urldecode($post_value));
                                if ($form->values[$post_key]->value!=$post_value){
                                    //Mark as changed
                                    $form->values[$post_key]->changed=true;
                                    //Before we can change it we see if the object stored at $form->values[$post_key] has a processor class name stated
                                    //This is used to validate or perform extra functions, such as resizing an image
                                    $serverdata=$form->values[$post_key];
                                    //See if this field has a processor - if so set it up, run it and change the value accordingly
                                    if ($serverdata->processorclass){
                                        //Get the processor class name
                                        $procname=$serverdata->processorclass;
                                        //Create a processor
                                        $proc=new $procname;
                                        //Run the processor and reassign it's result to $post_value
                                        $post_value=$proc->parseValue($post_value,$serverdata->fieldinfo);
                                    }
                                    if(iddi::$debug) iddiDebug::message("updating {$post_key} to {$post_value}");
                                }
                                //Finally update the form value

                                $form->values[$post_key]->value=$post_value;
                            }

                            //Update the entities from the current form
                            if($form->values){
                                foreach($form->values as $form_value){
                                    $ent=iddiRequest::getDataSource($form_value->entityid);
                                    if ($ent){
                                        $form_key=$form_value->fieldname;
                                        $form_val=$form_value->value;
                                        $form_val=urldecode($form_val);
                                        //Assign the value directly to the entity object
                                        $ent->$form_key=$form_val;
                                        //If we are updating the entity name process it
                                        if ($form_key=='entityname'){
                                            $ent->entityname=$form_val;
                                            iddiRequest::getForm()->updateEntityName($form_value->entityid,$form_val);
                                            iddiDebug::dumpvar('Changing Entity Type - details contain entity',$ent->dump(2));
                                        }
                                        //If we are changing the virtual filename of the entity update that on the form
                                        if ($form_key=='virtualfilename') $form->virtualfilename=$val;

                                    }else{
                                        iddiDebug::message('No Entity Found for '.$form_value->entityid);
                                    }
                                }
                            }
                          }
                      }

                      //Now process the context
                      switch ($this->getContext()){
                          case 'ajax':
                               break;
                          case 'getevents':
                               die(iddiJavascriptEvent::getEvents());
                               break;
                          case 'generateimage':
                               $im=new iddiImageField();
                               echo $im->createForEditor();
                               die();
                          case 'offline_rss':
                                echo 'Processing'.str_repeat(' ',1000)."\n\n\n\n";
                                flush();
                                //Go pull in the rss feed - no one is going to be seeing this request - it comes from an asyncronous RSS feed
                                iddiRSS::getFeed(urldecode($_REQUEST['url']),urldecode($_REQUEST['target']));
                                //No respose required here
                               die();
                          case 'getform':
                               echo iddiRequest::getform()->json();
                               die();
                          case 'tool':
                               $response->template=new idditemplate($this->source.'.html',self::getCurrentDataSource());
                               if ($this->section){
                                $r=$response->template->data->xpath("//tool[@id='".$this->tool."']//*[@id='".$this->section."']")->first();
                               }else{
                                $r=$response->template->data->xpath("//tool[@id='".$this->tool."']")->first();
                               }
                               if ($r==null){
                                    throw new iddiException('Tool '.$this->tool.' not found in '.$this->source,'iddi.request.toolrequest.toolnotfound',$this);
                               }
                               $r->preparse();
                               $r->parse();
                               $response->template->data=$r;
                               if($this->ismainrequest) $response->addError('The child request has become a main request','fatal');
                              //$request->page->template->data=$r;
                              break;
                          case 'debug':
                              $response->content=print_rp($this->getform(),true);
                              break;
                          case 'field-update':
                              $request=self::getMainRequest();
                              $fieldid=str_replace('postfield_','',$_REQUEST['field']);
                              $value=$_REQUEST['value'];
                              $form=iddiRequest::getform();
                              $field=$form->values[$fieldid];
                              if ($field){
                                  $field->value=$value;
                                  $fieldname=$field->fieldname;
                                  //Special Fields
                                  if ($field->fieldname=='entityname'){
                                      $entid=$field->entityid;
                                      foreach($form->values as $val){
                                          if ($val->entityid==$entid) $val->entityname=$value;
                                      }
                                  }
                              }
                              foreach($form->values as $field){
                                  $fieldname=$field->fieldname;
                                  $entity=self::getDataSource($field->entityid);
                                  $entity->setValue($fieldname,$field->value);
                              }
                              die('TEST:'.iddiJavascriptEvent::getEventsScript());
                              break;
                          case 'create':
                              //$this->makeMainRequest();
                              if(!$_POST){
                                  $this->makeMainRequest();
                                  $page=iddiRequest::addDataSource(new iddiPage());
                                  $response->template=new idditemplate('',$page);
                              }else{
                                  iddimysql::savefrompost();
                                  $f=iddiRequest::getform();
                                  $filename=$f->getField('virtualfilename')->value;
                                  header('Location: '.$filename);
                              }
                              break;
                          case 'save':
                              //Destroy the cache - first person in to each page will regenerate it
                              @rmdir('cache');
                              $request=self::getMainRequest();
                              //foreach($request->pages as $page){
                                  //$page->Save();
                              //}
                              iddiMySql::savefrompost();
                              //Redirect to the main page
                              /*
                              if (iddiRequest::getform()->templatechanged){
                                $filename=iddiRequest::getform()->virtualfilename;
                                echo "<script type=\"text/javascript\">document.location='".$filename.";</script>";
                              } */

                              break;
                          case 'deepcopy':
                              $this->getMainRequest()->page->deep_copy();
                              break;
                          case 'delete':
                              $this->getMainRequest()->page->delete();
                              break;
                          case 'restore':
                              $this->getMainRequest()->page->restore();
                              break;
                          case 'logout':
                              foreach($_SESSION as $k=>$v) unset($_SESSION[$k]);
                              header('location: '.$this->pageuri.'?recache=1');
                              die();
                              break;
                          case 'compile':
                              //$t=iddiTemplateList::loadall();
                              iddiTemplateList::compileall();
                              break;
                          case 'currentpage':
                              $response->template=self::getMainRequest()->page->getTemplate();
                              break;
                          default:
                              $this->indefault=true;
                              $this->makeMainRequest();
                              //Set Options
                              $mode=iddiRequest::getMode();

                              //Process posted data
                              if($_POST) iddimysql::savefrompost();
                      }
                      if($this->indefault){
                          //If we're in edit mode and have no context grab the iframe template for the editor
                          if($mode=='edit' && $this->getContext()=='' && $_GET['context']==''){
                              echo iddiResource::getResource('adminsystem')->render();
                              die();
                          }else{
                              if ($response->content==''){
                                  try {
                                      $page=iddiRequest::addDataSource(new iddiPage());
                                      $page->LoadPage($this->pageuri);
                                      $response->template=$page->getTemplate();
                                  }catch(iddiException $e){
                                      switch($e->code){
                                        case 'iddi.data.loadpagebyid.entitynotfound':
                                        case 'iddi.data.loadpagebyvf.pagenotfound':
                                          try {
                                              $page=iddiRequest::addDataSource(new iddiPage());
                                              $page->LoadPage('/errors/404');
                                              $response->template=$page->getTemplate();
                                          }catch(iddiException $e){
                                              switch($e->code){
                                                case 'iddi.data.loadpagebyid.entitynotfound':
                                                case 'iddi.data.loadpagebyvf.pagenotfound':
                                                die('<h1>There was a problem</h1><p>Unfortunatly the page ('.$this->pageuri.') you were looking for was not found.</p>');
                                                break;
                                                default:
                                                    echo $e;
                                              }
                                          }
                                          break;
                                        case 'iddi.data.loadpagebyid.entitydeleted':
                                          header('HTTP/1.1 410 Gone','410');
                                          try {
                                              $page=iddiRequest::addDataSource(new iddiPage());
                                              $page->LoadPage('/errors/410-Gone');
                                              $response->template=$page->getTemplate();
                                          }catch(iddiException $e){
                                              switch($e->code){
                                                case 'iddi.data.loadpagebyid.entitynotfound':
                                                case 'iddi.data.loadpagebyvf.pagenotfound':
                                                die('<h1>There was a problem</h1><p>Unfortunatly the page you were looking for is no longer available.</p>');
                                                break;
                                                default:
                                                    echo $e;
                                              }
                                          }
                                          break;
                                          default:
                                             echo $e;
                                      }
                                  }
                              }
                          }
                      }
                  }catch(Exception $e){
                      $response->addError($e,'getresponse.exception','fatal');
                      throw new iddiException($e->getMessage(),'fatal.other.'.$e->getCode());
                  }
              }
          }

          $e=$this->trigger(self::AFTER_GET_RESPONSE_BEFORE_REPLY);
          return $response;
      }
      function toXml($parent=null,$array=null){
          if($array==null) $array=$_REQUEST;
          if ($parent==null){
            $doc=new iddiXmlDocument();
            $parent=new iddiXmlNode($doc,'request',$doc,null);
          }else{
              $doc=$parent->owner;
          }

          foreach($_REQUEST as $k=>$v){
              $n=new iddiXmlIddiNode($doc,$k,$parent,null);
              if (is_array($v)){
                  foreach($v as $a){
                      $this->toXml($doc,$a);
                  }
              }else{
                  $n->value=$v;
              }
          }
          return $doc;
      }
    }
