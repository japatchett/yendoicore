<?php
//die(urlencode('../iddi-project/images/originals/fisheries/7048/fishing-in-norway2.jpg'));

    class iddiRequest_api_image extends Api_Result{
        function getResponse(){
            //Unlike other API calls we can specify to just return the image filename rather than a full api response
            $width=$_GET['width'];
            $height=$_GET['height'];
            $filename=urldecode($_GET['filename']);
            if($_GET['scalemode']) $scalemode=$_GET['scalemode'];
            else $scalemmode='force';
            $data=new stdClass();
            $data->width=$width;
            $data->height=$height;
            $data->scalemode=$scalemode;



            $code=base64_encode(serialize($data));
//die($code);
            $fileparts=pathinfo($filename);
            $filename_parts=explode('{',$fileparts['filename']);
            $filename2=$filename_parts[0];

            $in_filename='..'.str_replace('public','originals',$fileparts['dirname']).'/'.$filename2.'.'.$fileparts['extension'];

            if(!file_exists($in_filename)){
                $in_filename = '../iddi-project/images/originals/general/no-pic.jpg';
                $out_filename = 'iddi-project/images/originals/general/no-pic.jpg';
            }
            else $out_filename=str_replace('originals','public',$fileparts['dirname']).'/'.$filename2.'{'.$code.'}.'.$fileparts['extension'];
            $save_filename='..'.$out_filename;

            if(!file_exists($save_filename) || filemtime($in_filename) > filemtime($save_filename)){

                $image=new iddiImage($in_filename,$height,$width,$scalemode,true);
                $image->resize($height, $width,$scalemode);
                $image->save($save_filename);
            }
            //else {
            //    $image=new iddiImage($save_filename,$height,$width,$scalemode);
           // }

            switch($_GET['outmode']){
                case 'image':
                    $im=imagecreatefromjpeg($save_filename);
                    //Output the raw image file
                    header('Content-Type: image/jpeg');
                    imagejpeg($im);
                    die();
                    break;
                case 'json':
                    $image_attr=new stdClass();
                    $image_attr->outputwidth=$image->outputwidth;
                    $image_attr->outputheight=$image->outputheight;
                    $image_attr->path = str_replace('..','',$out_filename);
                    $image_attr->path = str_replace('//','/',$image_attr->path);
                    die(json_encode($image_attr));
                    break;
                default:
                    die(str_replace('//','/',str_replace('..','',$out_filename)));
            }
        }
    }