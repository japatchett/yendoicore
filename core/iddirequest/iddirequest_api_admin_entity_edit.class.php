<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_edit extends Api_Result{
    function output(){
        $e=new iddiEntityDefinition($_GET['e']);
        foreach($e->fielddefs as $field){
            $fields[$field->group][$field->type][]=$field;
        }

        $sql='SELECT * FROM iddi_sysfilenames f RIGHT JOIN {PREFIX}'.$_GET['e'].' t ON f.id=t.id AND f.entityname="'.$_GET['e'].'" WHERE t.id='.$_GET['i'];

        $d=iddiMySQL::query($sql);
        $item=$d->getFirstRow();

        $buttons='<div><i class="fa fa-save" id="iddi-form-save"><span>Save</span></i></div>';

        if($item->virtualfilename!=''){
            $buttons.='<div><a href="'.$item->virtualfilename.'"><i class="fa fa-edit"><span>Edit</span></i></a></div>';
        }
        if($e->entityname=='order'){
            $buttons.='<div><a href="/shop/invoice?id='.$item->id.'&context=view" target="blank"><i class="fa fa-print"><span>Invoice</span></i></a></div>';
        }

        if($item->pagetitle!=''){
            $out='<h1>Edit '.$e->name.' "'.$item->pagetitle.'"'.$buttons.'</h1>';
        }else{
            $out='<h1>Edit '.$e->name.' number '.$item->id.'"'.$buttons.'</h1>';
        }

        $out.='<form>';
        $out.='<input type="hidden" name="entityname" value="'.$_GET['e'].'"/>';
        $out.='<input type="hidden" name="entityid" value="'.$_GET['i'].'"/>';
        //Show all text types first
        foreach($fields as $group=>$groupfields){
            $main_item.='<h2>'.$group.'</h2>';
            if($groupfields['text'][0]){
                $main_item.='<fieldset>';
                foreach($groupfields['text'] as $f){
                    $main_item.='<label class="iddi-text-field"><span>'.$f->caption.'</span><input type="text" name="'.$f->fieldname.'" value="'.$item->dbfields[$f->fieldname].'"/></label>';
                }
                $main_item.='</fieldset>';
            }

            //Then numeric
            if($groupfields['number'][0]){
                $main_item.='<fieldset>';
                foreach($groupfields['number'] as $f){
                    $main_item.='<label class="iddi-numeric-field"><span>'.$f->caption.'</span><input type="text" name="'.$f->fieldname.'" value="'.$item->dbfields[$f->fieldname].'"/></label>';
                }
                $main_item.='</fieldset>';
            }

            //Then bool


            //Then lookup

            if($groupfields['lookup'][0]){
                $main_item.='<fieldset>';
                foreach($groupfields['lookup'] as $f){

                    /**
                     * Get values
                     */
                    $lookups=array('0'=>'');
                    $lookups=iddiCache::get('LOOKUPSA_'.$f->fieldname);
                    if(!$lookups){
                        $lookups=array('0'=>'');
                        $sql='SELECT id,pagetitle FROM iddi_sysfilenames WHERE entityname="'.iddiMySql::tidyname($f->lookup).'"';
                        $r=iddiMySql::query($sql);
                        foreach($r as $row) $lookups[$row->id]=$row->pagetitle;
                        iddiCache::save('LOOKUPSA_'.$f->fieldname, $lookups);
                    }

                    if(sizeof($lookups)<8){
                        $main_item.='<div class="iddi-label iddi-lookup-field"><span>'.$f->caption.'</span>';
                        foreach($lookups as $id=>$lookupitem){
                            if($id>0){
                                $main_item.='<label class="iddi-radio"><input type="radio" value="'.$id.'" name="'.$f->fieldname.'"';
                                if($id==$item->dbfields[$f->fieldname]) $main_item.=' checked="checked"';
                                $main_item.='/><span>'.$lookupitem.'</span></label>';
                            }
                        }
                        $main_item.='</div>';
                    }else{
                        $main_item.='<label class="iddi-lookup-field"><span>'.$f->caption.'</span>';

                        $main_item.='<select name="'.$f->fieldname.'">';

                        foreach($lookups as $id=>$lookupitem){
                            $main_item.='<option value="'.$id.'"';
                            if($id==$item->dbfields[$f->fieldname]) $main_item.=' selected="selected"';
                            $main_item.='>'.$lookupitem.'</option>';
                        }

                        $main_item.='</select>';
                        $main_item.='</label>';
                    }
                    //<input type="text" name="'.$f->fieldname.'" value="'.$item->dbfields[$f->fieldname].'"/>

                }
                $main_item.='</fieldset>';
            }

            //Then content

            if($groupfields['html'][0]){
                $main_item.='<fieldset>';
                foreach($groupfields['html'] as $f){
                    $main_item.='<label class="iddi-content-field"><span>'.$f->caption.'</span>
                                    <textarea name="'.$f->fieldname.'">'.$item->dbfields[$f->fieldname].'</textarea>
                                 </label>';
                }
                $main_item.='</fieldset>';
            }


            //Then images
        }


        //Sub Items
        $sql='SELECT entityname,fieldname,caption FROM iddi_sysentityfields WHERE lookup="'.$_GET['e'].'" ORDER BY entityname';
        $subitem_rs=iddiMySql::query($sql);
        $previous_entity='';
        if($subitem_rs->hasData()){
            foreach($subitem_rs as $subitem){
                $fields=array();
                $e=new iddiEntityDefinition($subitem->entityname);
                foreach($e->fielddefs as $field){
                    $fields[$field->type][]=$field;
                }


                $tabs[$subitem->entityname]->entityname=$subitem->entityname;

                $item_sql='SELECT * FROM iddi_sysfilenames f RIGHT JOIN {PREFIX}'.$subitem->entityname.' t ON f.id=t.id AND f.entityname="'.$subitem->entityname.'" WHERE (f.deleted is null or f.deleted=0) AND t.'.$subitem->fieldname.'='.$_GET['i'].' ORDER BY f.odr, t.id DESC LIMIT 0,200';

                $d=iddiMySQL::query($item_sql);
                $subitem_content='';
                //$subitem_content='<h2>'.$subitem->caption.'</h2>';
                if($previous_entity!=$subitem->entityname){
                    if($previous_entity==''){
                        $subitem_content.='</table>';
                    }
                    $subitem_content.='<table><tr>';
                    $subitem_content.='<th></th>';
                    if(isset($fields['image'])){
                        $subitem_content.='<th>'.$fields['image'][0]->caption.'</th>';
                    }
                    foreach($fields['text'] as $f){
                        $subitem_content.='<th>'.$f->caption.'</th>';
                    }
                    foreach($fields['number'] as $f){
                        $subitem_content.='<th>'.$f->caption.'</th>';
                    }
                    $subitem_content.='</tr>';
                }
                foreach($d as $row){
                    $subitem_content.='<tr class="iddi-show-item" data-entity-id="'.$row->dbfields['id'].'" data-entity-name="'.$subitem->entityname.'">';

                    //Show ID and pagetitle
                    $subitem_content.='<td>'.$row->dbfields['id'].'</td>';

                    if(isset($fields['image'])){
                        $subitem_content.='<td><img src="/api/image?width=80&height=80&scalemode=force&amp;outmode=image&amp;filename='.urldecode($row->dbfields[$fields['image'][0]->fieldname]).'"/></td>';
                    }


                    //Show all text types first
                    foreach($fields['text'] as $f){
                        $subitem_content.='<td>'.$row->dbfields[$f->fieldname].'</td>';
                    }

                    //Then numeric
                    foreach($fields['number'] as $f){
                        $subitem_content.='<td>'.$row->dbfields[$f->fieldname].'</td>';
                    }

                    //Then bool

                    //Then lookup

                    $subitem_content.='</tr>';
                }

                $previous_entity=$subitem->entityname;

                $tabs[$subitem->entityname]->content.=$subitem_content;
            }

            $out.='<div class="iddi-form-tabs"><ul>';
            $li='<li class="active">';
            if($main_item!=''){
                $out.=$li.'Main Entry</li>';
                $li='<li>';
            }
            foreach($tabs as $tab){
                $out.=$li.$tab->entityname.'</li>';
                $li='<li>';
            }
            $out.='</ul><div id="iddi-form-tab-content">';
            $div='<div>';
            if($main_item!=''){
                $out.='<div>'.$main_item.'</div>';
                $div='<div style="display:none">';
            }
            foreach($tabs as $tab){
                $out.=$div.$tab->content.'</table></div>';
            }
            $out.='</div></div>';
        }else{
            $out.=$main_item;
        }
        $out.='</form>';
        die($out);
    }
}